﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;

namespace SaleOfRoom.Common.Converters.Tests
{
    [TestClass()]
    public class XParseTests
    {
        [XRoot("realty-feed", "http://webmaster.yandex.ru/schemas/feed/realty/2010-06")]
        public class ContainerTest
        {
            [XName("offer")]
            public ResidentialTest Offer { get; set; }
        }

        public class ResidentialTest
        {
            [XAttribute("internal-id")]
            public string InternalId { get; set; }

            [XName("testNull")]
            public int? TestNull { get; set; }

            [XName("type")]
            public string Type { get; set; }

            [XIgnore]
            public DateTime CreationDate { get; set; }
            [XName("creation-date")]
            public string CreationDateString
            {
                get => CreationDate.ToString("YYYY-MM-DDTHH:mm:ss+00:00");
                set => CreationDate = DateTime.Parse(value);
            }

            public class LocationTest
            {
                [XName("country")]
                public string Country { get; set; } = "Россия";

                public class MetroTest
                {
                    [XName("name")]
                    public string Name { get; set; }
                }
                [XName("metro")]
                public MetroTest Metro { get; set; }
            }
            [XName("location")]
            public LocationTest Location { get; set; }

            [XList("image")]
            public List<string> Image { get; set; }
        }

        [TestMethod()]
        public void SerializeTest()
        {
            ContainerTest test = new ContainerTest
            {
                Offer = new ResidentialTest
                {
                    InternalId = "2345",
                    TestNull = 1,
                    Type = null,
                    CreationDate = DateTime.Now,
                    Location = new ResidentialTest.LocationTest
                    {
                        Metro = new ResidentialTest.LocationTest.MetroTest
                        {
                            Name = "metro"
                        }
                    },
                    Image = new List<string>
                    {
                        "img1",
                        "img2",
                        "img3"
                    }
                }
            };

            using (XParse parse = new XParse())
            {
                string result = parse.Serialize(test);
                Assert.IsNotNull(result);
            }
        }
    }
}