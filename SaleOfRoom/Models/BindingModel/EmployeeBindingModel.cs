﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.BindingModel
{
    public class AddEmployeeBindingModel
    {
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string SecondName { get; set; }

        [Display(Name = "Отчество")]
        public string LastName { get; set; }
        
        [Display(Name = "Должность")]
        public string Position { get; set; }
    }

    public class EmployeeEditBindingModel
    {
        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public string SecondName { get; set; }

        [Required]
        [Display(Name = "Отчество")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Должность")]
        public string Position { get; set; }

        public bool? DeleteImage { get; set; }
    }
}