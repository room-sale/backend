﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.BindingModel
{
    public class AddEstateBindingModel
    {
        [Display(Name = "Этаж")]
        public int? Floor { get; set; }

        [Required]
        [Display(Name = "Площадь")]
        public double? Area { get; set; }

        [Display(Name = "Жилая площадь")]
        public double? LivingArea { get; set; }

        [Display(Name = "Площадь кухни")]
        public double? KitchenArea { get; set; }

        [Display(Name = "Количество комнат")]
        public int? RoomCount { get; set; }

        [Required]
        [Display(Name = "Цена")]
        public double? Price { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        [Required]
        [Display(Name = "Статус недвижимости")]
        public string ReadyState { get; set; }

        [Required]
        [Display(Name = "Регион")]
        public string Region { get; set; }

        [Required]
        [Display(Name = "Район")]
        public string District { get; set; }

        [Required]
        [Display(Name = "Город")]
        public string City { get; set; }

        [Display(Name = "Округ")]
        public string CityDistrict { get; set; }

        [Required]
        [Display(Name = "Улица")]
        public string Street { get; set; }

        [Required]
        [Display(Name = "Номер дома")]
        public int? HouseNumber { get; set; }

        [Display(Name = "Корпус")]
        public int? HousingNumber { get; set; }

        [Display(Name = "Номер картиры")]
        public int? FlatNumber { get; set; }

        [Required]
        [Display(Name = "Риелтор")]
        public string RealtorId { get; set; }

        [Required]
        [Display(Name = "Контрагент")]
        public Guid? CounterpartyId { get; set; }

        [Required]
        [Display(Name = "Категория")]
        public int? CategoryId { get; set; }

        [Required]
        [Display(Name = "Тип объявления")]
        public int? TransactionTypeId { get; set; }

        [Required]
        [Display(Name = "Материал стен")]
        public int? WallMaterialId { get; set; }

        [Required]
        [Display(Name = "Координаты")]
        public string Coord { get; set; }

        [Required]
        [Display(Name = "Заголовок")]
        public string Header { get; set; }

        [Required]
        [Display(Name = "Текст")]
        public string Message { get; set; }

        [Display(Name = "Тип комнат")]
        public string RommType { get; set; }

        [Display(Name = "Статус")]
        public string Status { get; set; }

        [Display(Name = "Этажность")]
        public int? StoreysNumber { get; set; }

        [Required]
        [Display(Name = "Комиссия")]
        public double? Commission { get; set; }

        [Display(Name = "Ремонт")]
        public string Repairs { get; set; }
        
        [Display(Name = "Кадастровый номер")]
        public string CadastralNumber { get; set; }

        [Display(Name = "Тип сделки")]
        public string DealStatus { get; set; }

        [Display(Name = "Выгрузка: ДомКлик")]
        public bool? OnDomClick { get; set; }
        [Display(Name = "Выгрузка: Рестат")]
        public bool? OnRestat { get; set; }
        [Display(Name = "Выгрузка: Мир квартир")]
        public bool? OnMirKvartir { get; set; }
        [Display(Name = "Выгрузка: Где этот дом")]
        public bool? OnGdeEtotDom { get; set; }
    }

    public class EstateEditBindingModel
    {
        [Display(Name = "Этаж")]
        public int? Floor { get; set; }

        [Required]
        [Display(Name = "Площадь")]
        public double? Area { get; set; }
        
        [Display(Name = "Жилая площадь")]
        public double? LivingArea { get; set; }

        [Display(Name = "Площадь")]
        public double? KitchenArea { get; set; }

        [Required]
        [Display(Name = "Количество")]
        public int? RoomCount { get; set; }

        [Required]
        [Display(Name = "Цена")]
        public double? Price { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        [Required]
        [Display(Name = "Статус недвижимости")]
        public string ReadyState { get; set; }

        [Required]
        [Display(Name = "Регион")]
        public string Region { get; set; }

        [Required]
        [Display(Name = "Район")]
        public string District { get; set; }

        [Required]
        [Display(Name = "Город")]
        public string City { get; set; }

        [Display(Name = "Округ")]
        public string CityDistrict { get; set; }

        [Required]
        [Display(Name = "Улица")]
        public string Street { get; set; }

        [Required]
        [Display(Name = "Номер дома")]
        public int? HouseNumber { get; set; }

        [Display(Name = "Корпус")]
        public int? HousingNumber { get; set; }

        [Display(Name = "Номер квартиры")]
        public int? FlatNumber { get; set; }

        [Required]
        [Display(Name = "Риелтор")]
        public string RealtorId { get; set; }

        [Display(Name = "Контрагент")]
        public Guid CounterpartyId { get; set; }

        [Required]
        [Display(Name = "Категория")]
        public int? CategoryId { get; set; }

        [Required]
        [Display(Name = "Тип объявления")]
        public int? TransactionTypeId { get; set; }

        [Required]
        [Display(Name = "Материал стен")]
        public int? WallMaterialId { get; set; }

        [Required]
        [Display(Name = "Координаты")]
        public string Coord { get; set; }

        [Display(Name = "Цена за метр квадратный")]
        public double? PriceArea { get; set; }

        [Required]
        [Display(Name = "Заголовок")]
        public string Header { get; set; }

        [Required]
        [Display(Name = "Сообщение")]
        public string Message { get; set; }

        [Display(Name = "Тип комнат")]
        public string RommType { get; set; }

        [Display(Name = "Статус")]
        public string Status { get; set; }

        [Display(Name = "Этажность")]
        public int? StoreysNumber { get; set; }

        [Required]
        [Display(Name = "Комиссия")]
        public double? Commission { get; set; }

        [Display(Name = "Ремонт")]
        public string Repairs { get; set; }

        [Display(Name = "Кадастровый номер")]
        public string CadastralNumber { get; set; }

        [Display(Name = "Изображения")]
        public Guid[] DeletedImages { get; set; }

        [Display(Name = "Тип сделки")]
        public string DealStatus { get; set; }

        [Display(Name = "Выгрузка: ДомКлик")]
        public bool? OnDomClick { get; set; }
        [Display(Name = "Выгрузка: Рестат")]
        public bool? OnRestat { get; set; }
        [Display(Name = "Выгрузка: Мир квартир")]
        public bool? OnMirKvartir { get; set; }
        [Display(Name = "Выгрузка: Где этот дом")]
        public bool? OnGdeEtotDom { get; set; }
    }
}