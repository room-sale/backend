﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.ViewModel
{
    public class EmployeePageViewModel
    {
        public IEnumerable<EmployeeIndexViewModel> Employees { get; set; }
        public int EmployeeCount { get; set; }
    }

    public class EmployeeIndexViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public Guid Picture { get; set; }
    }

    public class EmployeeViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public Guid Picture { get; set; }
    }
}