﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.ViewModel
{
    public class IndexCounterpartyViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int EstateCount { get; set; }
    }

    public class CounterpartyPageViewModel
    {
        public IEnumerable<IndexCounterpartyViewModel> Counterparties { get; set; }
        public int CounterpartyCount { get; set; }
    }

    public class CounterpartyViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public IEnumerable<EstateTableViewModel> Estates { get; set; }
    }
}