﻿using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.ViewModel
{
    public class IndexRealtorViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }

    public class RealtorViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public IEnumerable<EstateTableViewModel> Estates { get; set; }
    }

    public class RealtorPageViewModel
    {
        public IEnumerable<IndexRealtorViewModel> Realtors { get; set; }
        public int RealtorCount { get; set; }
    }
}