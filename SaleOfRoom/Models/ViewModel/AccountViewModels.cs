﻿using System;
using System.Collections.Generic;

namespace SaleOfRoom.Models.ViewModel
{
    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public string Role { get; set; }

        public string Id { get; set; }
    }

    public class IndexUserViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
    }

    public class UserPageViewModel
    {
        public IEnumerable<IndexUserViewModel> Users { get; set; }

        public int UsersCount { get; set; }
    }

    public class UserViewModel
    {
        public string Id { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
