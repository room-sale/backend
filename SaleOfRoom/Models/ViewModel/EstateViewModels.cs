﻿using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.ViewModel
{
    public class IndexEstateViewModel
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public int? Floor { get; set; }
        public double? Area { get; set; }
        public double? LivingArea { get; set; }
        public double? KitchenArea { get; set; }
        public int? RoomCount { get; set; }
        public double? Price { get; set; }
        public double? PriceArea { get; set; }
        public string CreatedAt { get; set; }
        public string Comment { get; set; }
        public string ReadyState { get; set; }
        public string Coord { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string CityDistrict { get; set; }
        public string Street { get; set; }
        public int? HouseNumber { get; set; }
        public int? FlatNumber { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
        
        public Guid[] Pictures { get; set; }
        public object Counterparty { get; set; }
        public IndexRealtorViewModel Realtor { get; set; }
    }

    public class EstatePageViewModel
    {
        public object Estates { get; set; }
        public int EstateCount { get; set; }
    }

    public class EstateViewModel
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public int? Floor { get; set; }
        public double? Area { get; set; }
        public double? LivingArea { get; set; }
        public double? KitchenArea { get; set; }
        public int? RoomCount { get; set; }
        public double? Price { get; set; }
        public double? PriceArea { get; set; }
        public string CreatedAt { get; set; }
        public string Comment { get; set; }
        public string ReadyState { get; set; }
        public Category Category { get; set; }
        public TransactionType TransactionType { get; set; }
        public WallMaterial WallMaterial { get; set; }
        public Guid[] Pictures { get; set; }
        public string Coord { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string CityDistrict { get; set; }
        public string Street { get; set; }
        public int? HouseNumber { get; set; }
        public int? HousingNumber { get; set; }
        public int? FlatNumber { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
        public string RommType { get; set; }
        public string Status { get; set; }
        public int? StoreysNumber { get; set; }
        public double? Commission { get; set; }
        public string Repairs { get; set; }
        public string CadastralNumber { get; set; }
        public object Counterparty { get; set; }
        public string DealStatus { get; set; }
        public bool? OnDomClick { get; set; }
        public bool? OnRestat { get; set; }
        public bool? OnMirKvartir { get; set; }
        public bool? OnGdeEtotDom { get; set; }
        public IndexRealtorViewModel Realtor { get; set; }
    }

    public class EstateTableViewModel
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public string Category { get; set; }
        public string Address { get; set; }
        public string Info { get; set; }
        public double? Price { get; set; }
        public string CreatedAt { get; set; }
        public Guid[] Pictures { get; set; }
        public IndexRealtorViewModel Realtor { get; set; }
    }
}