﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.ViewModel
{
    public class HistoryIndexViewModel
    {
        public string Action { get; set; }
        public string TableName { get; set; }
        public string PrimaryKey { get; set; }
        public string ColumnName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime Date { get; set; }
        public object User { get; set; }
    }

    public class HistoryPageViewModel
    {
        public IEnumerable<HistoryIndexViewModel> History { get; set; }
        public int HistoryCount { get; set; }
    }
}