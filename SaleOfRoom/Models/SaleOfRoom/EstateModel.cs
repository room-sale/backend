﻿using SaleOfRoom.Common.Attributes;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public partial class Estate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public bool isDisabled { get; set; }
        /// <summary>
        /// ID для пользователя
        /// </summary>
        public int Number { get; private set; }
        /// <summary>
        /// Этаж
        /// </summary>
        public int? Floor { get; set; }
        /// <summary>
        /// Общая площадь жилья
        /// </summary>
        public double? Area { get; set; }
        /// <summary>
        /// Жилая площадь
        /// </summary>
        public double? LivingArea { get; set; }
        /// <summary>
        /// Площадь кухни
        /// </summary>
        public double? KitchenArea { get; set; }
        /// <summary>
        /// Количество комнат
        /// </summary>
        public int? RoomCount { get; set; }
        /// <summary>
        /// Цена, руб
        /// </summary>
        public double? Price { get; set; }
        /// <summary>
        /// Цена за метр квадратный
        /// </summary>
        public double? PriceArea { get; set; }
        /// <summary>
        /// Дата создания объявления
        /// </summary>
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// Служебный комментарий
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Готовность объявления
        /// </summary>
        public string ReadyState { get; set; }
        /// <summary>
        /// Регион
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// Район
        /// </summary>
        public string District { get; set; }
        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Городской округ
        /// </summary>
        public string CityDistrict { get; set; }
        /// <summary>
        /// Улица
        /// </summary>
        public string Street { get; set; }
        /// <summary>
        /// Номер дома
        /// </summary>
        public int? HouseNumber { get; set; }
        /// <summary>
        /// Корпус
        /// </summary>
        public int? HousingNumber { get; set; }
        /// <summary>
        /// Номер квартиры
        /// </summary>
        public int? FlatNumber { get; set; }
        /// <summary>
        /// Координаты
        /// </summary>
        public string Coord { get; set; }
        /// <summary>
        /// Риелтор
        /// </summary>
        [ForeignKey("Realtor")]
        public string UserId { get; set; }
        /// <summary>
        /// Собственник (контрагент)
        /// </summary>
        public Guid CounterpartyId { get; set; }
        /// <summary>
        /// Категория
        /// </summary>
        public int? CategoryId { get; set; }
        /// <summary>
        /// Тип сделки (продажа/аренда)
        /// </summary>
        public int? TransactionTypeId { get; set; }
        /// <summary>
        /// Материал стен
        /// </summary>
        public int? WallMaterialId { get; set; }
        /// <summary>
        /// Заколовок
        /// </summary>
        public string Header { get; set; }
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Тип комнат
        /// </summary>
        public string RommType { get; set; }
        /// <summary>
        /// Статус
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Этажность
        /// </summary>
        public int? StoreysNumber { get; set; }
        /// <summary>
        /// Комиссия
        /// </summary>
        public double? Commission { get; set; }
        /// <summary>
        /// Ремонт
        /// </summary>
        public string Repairs { get; set; }
        /// <summary>
        /// Кадастровый номер
        /// </summary>
        public string CadastralNumber { get; set; }
        /// <summary>
        /// Тип сделки
        /// </summary>
        public string DealStatus { get; set; }

        /// <summary>
        /// Выгрузка на ДомКлик
        /// </summary>
        public bool? OnDomClick { get; set; }
        /// <summary>
        /// Выгрузка на Рестат
        /// </summary>
        public bool? OnRestat { get; set; }
        /// <summary>
        /// Выгрузка на Мир квартир
        /// </summary>
        public bool? OnMirKvartir { get; set; }
        /// <summary>
        /// Выгрузка на Где этот дом
        /// </summary>
        public bool? OnGdeEtotDom { get; set; }
        
        [Ignore]
        public virtual ICollection<Picture> Pictures { get; set; }
        [Ignore]
        public virtual Realtor Realtor { get; set; }
        [Ignore]
        public virtual Counterparty Counterparty { get; set; }
        [Ignore]
        public virtual Category Category { get; set; }
        [Ignore]
        public virtual TransactionType TransactionType { get; set; }
        [Ignore]
        public virtual WallMaterial WallMaterial { get; set; }

        public Estate()
        {
            Pictures = new List<Picture>();
        }
    }
}