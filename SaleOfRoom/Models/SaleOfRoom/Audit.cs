﻿using Microsoft.AspNet.Identity;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public class EntityLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Action { get; set; }
        public string TableName { get; set; }
        public string PrimaryKey { get; set; }
        public string ColumnName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime Date { get; set; }
        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        internal HistoryIndexViewModel ToIndexViewModel(string role)
        {
            return new HistoryIndexViewModel()
            {
                Action = Action,
                ColumnName = ColumnName,
                Date = Date,
                NewValue = NewValue,
                OldValue = OldValue,
                PrimaryKey = PrimaryKey,
                TableName = TableName,
                User = User.ToViewModel(role)
            };
        }

    }

    //public class AuditEntry
    //{
    //    public AuditEntry(DbEntityEntry entry)
    //    {
    //        Entry = entry;
    //    }
 
    //    public DbEntityEntry Entry { get; }
    //    public string TableName { get; set; }
    //    public string UserId { get; set; }
    //    public Dictionary<string, object> KeyValues { get; } = new Dictionary<string, object>();
    //    public Dictionary<string, object> OldValues { get; } = new Dictionary<string, object>();
    //    public Dictionary<string, object> NewValues { get; } = new Dictionary<string, object>();
 
    //    public EntityLog ToEntityLog()
    //    {
    //        return new EntityLog
    //        {
    //            EntityName = TableName,
    //            OperatedAt = DateTime.UtcNow,
    //            KeyValues  = JsonConvert.SerializeObject(KeyValues),
    //            OldValues  = OldValues.Count == 0 ? null : JsonConvert.SerializeObject(OldValues),
    //            NewValues  = NewValues.Count == 0 ? null : JsonConvert.SerializeObject(NewValues),
    //            UserId = UserId
    //        };
    //    }
    //}
}