﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class TransactionType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class WallMaterial
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}