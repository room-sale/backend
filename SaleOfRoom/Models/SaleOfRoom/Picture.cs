﻿using SaleOfRoom.Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public class Picture
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public byte[] Image { get; set; }
        
        public Guid? EstateId { get; set; }

        [Ignore]
        public virtual Estate  Estate { get; set; }
    }
}