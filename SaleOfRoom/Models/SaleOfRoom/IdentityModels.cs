﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SaleOfRoom.Common.Attributes;
using SaleOfRoom.Models.ViewModel;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public class ApplicationUser : IdentityUser
    {
        public bool isDisabled { get; set; }

        [Ignore]
        public virtual Realtor Realtor { get; set; }
        //public virtual ICollection<EntityLog> EntityLogs { get; set; }

        public ApplicationUser()
            : base()
        {
            //EntityLogs = new List<EntityLog>();
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public UserViewModel ToViewModel(string role)
        {
            return new UserViewModel() 
            {
                Id = Id,
                Email = Email,
                FirstName = this?.Realtor?.FirstName,
                SecondName = this?.Realtor?.SecondName,
                LastName = this?.Realtor?.LastName,
                FullName = this?.Realtor?.FirstName + " " + this?.Realtor?.SecondName + " " + this?.Realtor?.LastName,
                Phone = this?.Realtor?.Phone,
                Role = role
            };
        }
    }
}