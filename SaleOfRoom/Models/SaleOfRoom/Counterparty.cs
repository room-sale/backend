﻿using SaleOfRoom.Common.Attributes;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public class Counterparty
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public bool isDisabled { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }

        [Ignore]
        public virtual ICollection<Estate> Estates { get; set; }

        public Counterparty()
        {
            Estates = new List<Estate>();
        }

        public IndexCounterpartyViewModel ToIndexViewModel()
        {
            return new IndexCounterpartyViewModel()
            {
                Id = Id,
                Name = SecondName + " " + FirstName + " " + LastName,
                Phone = Phone,
                EstateCount = Estates.Count
            };
        }

        public CounterpartyViewModel ToViewModel()
        {
            return new CounterpartyViewModel()
            {
                Id = Id,
                FirstName = FirstName,
                SecondName = SecondName,
                LastName = LastName,
                Name = SecondName + " " + FirstName + " " + LastName,
                Phone = Phone,
                Estates = Estates.Select(x => x.ToEstateTableViewModel()),
            };
        }
    }
}