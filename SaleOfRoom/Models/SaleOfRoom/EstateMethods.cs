﻿using SaleOfRoom.Common;
using SaleOfRoom.Common.Converters;
using SaleOfRoom.Common.Datalayer;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public partial class Estate
    {
        public EstateViewModel ToViewModel(bool isRealtor, bool isAdmin, string id)
        {
            object counterparty;
            if (isRealtor && Realtor.UserId == id || isAdmin)
                counterparty = Counterparty.ToIndexViewModel();
            else
                counterparty = null;

            return new EstateViewModel()
            {
                Id = Id,
                Area = Area,
                Comment = Comment,
                Counterparty = counterparty,
                CreatedAt = CreatedAt.ToString("dd'/'MM'/'yyyy"),
                Floor = Floor,
                KitchenArea = KitchenArea,
                LivingArea = LivingArea,
                Number = Number,
                Price = Price,
                ReadyState = ReadyState,
                Realtor = Realtor.ToIndexViewModel(),
                RoomCount = RoomCount,
                Category = Category,
                WallMaterial = WallMaterial,
                TransactionType = TransactionType,
                Pictures = Pictures.Select(p => p.Id).ToArray(),
                Coord = Coord,
                City = City,
                CityDistrict = CityDistrict,
                District = District,
                FlatNumber = FlatNumber,
                HouseNumber = HouseNumber,
                HousingNumber = HousingNumber,
                Region = Region,
                Street = Street,
                PriceArea = PriceArea,
                Message = Message,
                Header = Header,
                CadastralNumber = CadastralNumber,
                Commission = Commission,
                Repairs = Repairs,
                RommType = RommType,
                Status = Status,
                StoreysNumber = StoreysNumber,
                DealStatus = DealStatus,
                OnDomClick = OnDomClick,
                OnRestat = OnRestat,
                OnMirKvartir = OnMirKvartir,
                OnGdeEtotDom = OnGdeEtotDom
            };
        }

        public IndexEstateViewModel ToIndexViewModel(bool isRealtor, bool isAdmin, string id)
        {
            if (Realtor == null)
                using (var datalayer = new Datalayer(new ApplicationDbContext()))
                    Realtor = datalayer.Realtors.Get(UserId);

            if (Counterparty == null)
                using (var datalayer = new Datalayer(new ApplicationDbContext()))
                    Counterparty = datalayer.Counterparties.Get(CounterpartyId);

            object counterparty;
            if (isRealtor && Realtor.UserId == id || isAdmin)
                counterparty = Counterparty.ToIndexViewModel();
            else
                counterparty = null;

            return new IndexEstateViewModel()
            {
                Id = Id,
                Area = Area,
                Comment = Comment,
                Counterparty = counterparty,
                CreatedAt = CreatedAt.ToString("dd'/'MM'/'yyyy"),
                Floor = Floor,
                KitchenArea = KitchenArea,
                LivingArea = LivingArea,
                Number = Number,
                Price = Price,
                ReadyState = ReadyState,
                Realtor = Realtor.ToIndexViewModel(),
                RoomCount = RoomCount,
                Pictures = Pictures.Select(p => p.Id).ToArray(),
                Coord = Coord,
                Street = Street,
                Region = Region,
                HouseNumber = HouseNumber,
                FlatNumber = FlatNumber,
                District = District,
                CityDistrict = CityDistrict,
                City = City,
                PriceArea = PriceArea,
                Header = Header,
                Message = Message
            };
        }

        public EstateTableViewModel ToEstateTableViewModel()
        {
            if (Realtor == null)
                using (var datalayer = new Datalayer(new ApplicationDbContext()))
                    Realtor = datalayer.Realtors.Get(UserId);

            string address = City + ", " + District + ", " + Street;
            if (HouseNumber.HasValue)
                address += ", д. " + HouseNumber.Value;
            if (HousingNumber.HasValue)
                address += ", к. " + HousingNumber;
            if (FlatNumber.HasValue)
                address += ", кв. " + FlatNumber.Value;

            return new EstateTableViewModel() 
            {
                Address = address,
                Realtor = Realtor.ToIndexViewModel(),
                Category = 
                    Category.Name 
                    + " " + RoomCount 
                    + " " + TransactionType.Name,
                CreatedAt = CreatedAt.ToString("dd'/'MM'/'yyyy"),
                Id = Id,
                Number = Number,
                Pictures = Pictures.Select(p => p.Id).ToArray(),
                Price = Price,
                Info = 
                    "Эт/эт-ть: " + Floor + "/" + StoreysNumber 
                    + " Метраж(об/ж/к)" + Area + "/" + LivingArea + "/" + KitchenArea
            };
        }

        public ResidentialEstateYRL ToResidentialEstateYRL()
        {
            List<string> images = new List<string>();
            if (Pictures == null)
                using (var datalayer = new Datalayer(new ApplicationDbContext()))
                    Pictures = datalayer.Picturies.GetAll().Where(p => p.EstateId == this.Id).ToArray();
            foreach (var picture in this.Pictures.ToArray())
                images.Add("https://mayak-reality.com/v1/api/service/image?id=" + picture.Id.ToString());

            return new ResidentialEstateYRL()
            {
                Floor = Floor ?? 0,
                Area = new ResidentialEstateYRL.EstateArea()
                {
                    Value = Area ?? 0,
                    Unit = "кв. м"
                },
                LivingSpace = new ResidentialEstateYRL.EstateArea()
                {
                    Value = LivingArea ?? 0,
                    Unit = "кв. м"
                },
                KitchenSpace = new ResidentialEstateYRL.EstateArea()
                {
                    Value = this.KitchenArea ?? 0,
                    Unit = "кв. м"
                },
                Rooms = RoomCount ?? 0,
                Price = new ResidentialEstateYRL.EstatePrice()
                {
                    Value = Price ?? 0 + Commission ?? 0,
                    Unit = "кв. м"
                },
                CreationDate = this.CreatedAt,
                Location = new ResidentialEstateYRL.EstateLocation()
                {
                    Country = "Россия",
                    Region = Region,
                    District = CityDistrict,
                    Address = Street + " " + HouseNumber.ToString() ?? 0.ToString(),
                    Apartment = FlatNumber.ToString() ?? 0.ToString(),
                    LocalityName = City,
                    SubLocalityName = District
                },
                SalesAgent = new ResidentialEstateYRL.EstateSalesAgent()
                {
                    Category = "агентство",
                    Email = Realtor.User.Email,
                    Name = Realtor.ToIndexViewModel().Name,
                    Organization = "Маяк",
                    Phone = Realtor.Phone,
                    Url = "https://mayak-reality.com"
                },

                //TODO: Category и остальное
                Category = Category.Name,
                BuildingType = WallMaterial.Name,
                //DisableFlatPlanGuess = "1",
                InternalId = "MRE_" + Number.ToString().PadLeft(15, '0'),
                Description = Message,
                RoomsType = RommType,
                FloorsTotal = StoreysNumber,
                //AgentFee = this.Commission.Value,
                Renovation = Repairs,
                CadastralNumber = CadastralNumber,
                DealStatus = DealStatus ?? "",
                Image = images,
        };
        }

        public static void AplyFilter(Filter filter, ref IEnumerable<Estate> items)
        {
            if (filter.AreaFrom.HasValue)
                items = items.Where(e => e.Area> filter.AreaFrom.Value);
            if (filter.AreaFrom.HasValue && filter.AreaTo.HasValue)
                items = items.Where(e => e.Area < filter.AreaTo.Value);

            if (filter.PriceFrom.HasValue)
                items = items.Where(e => e.Price > filter.PriceFrom.Value);
            if (filter.PriceFrom.HasValue && filter.PriceTo.HasValue)
                items = items.Where(e => e.Price < filter.PriceTo.Value);
            
            if (filter.CategoryId.HasValue)
                items = items.Where(e => e.CategoryId == filter.CategoryId.Value);
            if (filter.TransactionTypeId.HasValue)
                items = items.Where(e => e.TransactionTypeId == filter.TransactionTypeId.Value);
            if (filter.WallMaterialId.HasValue)
                items = items.Where(e => e.WallMaterialId == filter.WallMaterialId.Value);
            
            if (filter.City != null && filter.City.Length > 0)
                items = items.Where(e => e.City.ToLower().Contains(filter.City.ToLower()));
            
            if (filter.CityDistrict != null && filter.CityDistrict.Length > 0)
                items = items.Where(e => e.CityDistrict.ToLower().Contains(filter.CityDistrict.ToLower()));
            
            if (filter.District != null && filter.District.Length > 0)
                items = items.Where(e => e.District.ToLower().Contains(filter.District.ToLower()));
            
            if (filter.Region != null && filter.Region.Length > 0)
                items = items.Where(e => e.Region.ToLower().Contains(filter.Region.ToLower()));
            
            if (filter.Street != null && filter.Street.Length > 0)
                items = items.Where(e => e.Street.ToLower().Contains(filter.Street.ToLower()));
        }
    }
}