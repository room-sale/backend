﻿using SaleOfRoom.Common.Attributes;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public Guid? PictureId { get; set; }
        
        [Ignore]
        public Picture Picture { get; set; }

        public EmployeeIndexViewModel ToEmployeeIndexViewModel()
        {
            return new EmployeeIndexViewModel() 
            {
                Id = Id,
                Picture = PictureId ?? Guid.Empty,
                Name = SecondName + " " + FirstName + " " + LastName,
                Position = Position
            };
        }

        public EmployeeViewModel ToEmployeeViewModel()
        {
            return new EmployeeViewModel() 
            {
                Id = Id,
                Picture = PictureId ?? Guid.Empty,
                FirstName = FirstName,
                SecondName = SecondName,
                LastName = LastName,
                Position = Position
            };
        }
    }
}