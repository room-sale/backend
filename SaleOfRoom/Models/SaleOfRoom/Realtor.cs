﻿using SaleOfRoom.Common.Attributes;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Models.SaleOfRoom
{
    public class Realtor
    {
        [Key, ForeignKey("User")]
        public string UserId { get; set; }
        public bool isDisabled { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }

        [Ignore]
        public virtual ApplicationUser User { get; set; }
        [Ignore]
        public virtual ICollection<Estate> Estates { get; set; }
        
        public Realtor() 
        {
            Estates = new List<Estate>();
        }

        public IndexRealtorViewModel ToIndexViewModel()
        {
            return new IndexRealtorViewModel()
            { 
                Id = UserId,
                Name = SecondName + " " + FirstName + " " + LastName,
                Phone = Phone
            };
        }

        public RealtorViewModel ToViewModel()
        {
            return new RealtorViewModel()
            {
                Id = UserId,
                FirstName = FirstName,
                SecondName = SecondName,
                LastName = LastName,
                Name = SecondName + " " + FirstName + " " + LastName,
                Phone = Phone,
                Estates = Estates.Select(x => x.ToEstateTableViewModel())
            };
        }
    }
}