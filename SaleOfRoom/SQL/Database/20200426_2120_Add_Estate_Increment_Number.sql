﻿CREATE OR ALTER 
TRIGGER [dbo].[Estate_Increment_Number] 
ON [dbo].[Estates] 
AFTER INSERT AS 
BEGIN 
	UPDATE 
		Estates 
	SET 
		Number = (
			SELECT 
				ISNULL((
					SELECT MAX(Number) FROM Estates), 0) + 1) 
			FROM Inserted 
			WHERE 
				Estates.Id = Inserted.Id; 
END
GO