﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Counterparties",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        isDisabled = c.Boolean(nullable: false),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        LastName = c.String(),
                        Phone = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Estates",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        isDisabled = c.Boolean(nullable: false),
                        Number = c.Int(nullable: false),
                        Floor = c.Int(),
                        Area = c.Double(nullable: false),
                        LivingArea = c.Double(nullable: false),
                        KitchenArea = c.Double(nullable: false),
                        RoomCount = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        Comment = c.String(),
                        ReadyState = c.String(),
                        Region = c.String(),
                        District = c.String(),
                        City = c.String(),
                        CityDistrict = c.String(),
                        Street = c.String(),
                        HouseNumber = c.Int(nullable: false),
                        FlatNumber = c.Int(),
                        Coord = c.String(),
                        RealtorId = c.String(),
                        CounterpartyId = c.Guid(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        TransactionTypeId = c.Int(nullable: false),
                        WallMaterialId = c.Int(nullable: false),
                        Realtor_UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Counterparties", t => t.CounterpartyId, cascadeDelete: true)
                .ForeignKey("dbo.Realtors", t => t.Realtor_UserId)
                .ForeignKey("dbo.TransactionTypes", t => t.TransactionTypeId, cascadeDelete: true)
                .ForeignKey("dbo.WallMaterials", t => t.WallMaterialId, cascadeDelete: true)
                .Index(t => t.CounterpartyId)
                .Index(t => t.CategoryId)
                .Index(t => t.TransactionTypeId)
                .Index(t => t.WallMaterialId)
                .Index(t => t.Realtor_UserId);
            
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Image = c.Binary(),
                        EstateId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Estates", t => t.EstateId, cascadeDelete: true)
                .Index(t => t.EstateId);
            
            CreateTable(
                "dbo.Realtors",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        isDisabled = c.Boolean(nullable: false),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        LastName = c.String(),
                        Phone = c.String(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.EntityLogs",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Action = c.String(),
                        TableName = c.String(),
                        PrimaryKey = c.String(),
                        ColumnName = c.String(),
                        OldValue = c.String(),
                        NewValue = c.String(),
                        Date = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.TransactionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WallMaterials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Estates", "WallMaterialId", "dbo.WallMaterials");
            DropForeignKey("dbo.Estates", "TransactionTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Realtors", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EntityLogs", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Estates", "Realtor_UserId", "dbo.Realtors");
            DropForeignKey("dbo.Pictures", "EstateId", "dbo.Estates");
            DropForeignKey("dbo.Estates", "CounterpartyId", "dbo.Counterparties");
            DropForeignKey("dbo.Estates", "CategoryId", "dbo.Categories");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.EntityLogs", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Realtors", new[] { "UserId" });
            DropIndex("dbo.Pictures", new[] { "EstateId" });
            DropIndex("dbo.Estates", new[] { "Realtor_UserId" });
            DropIndex("dbo.Estates", new[] { "WallMaterialId" });
            DropIndex("dbo.Estates", new[] { "TransactionTypeId" });
            DropIndex("dbo.Estates", new[] { "CategoryId" });
            DropIndex("dbo.Estates", new[] { "CounterpartyId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.WallMaterials");
            DropTable("dbo.TransactionTypes");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.EntityLogs");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Realtors");
            DropTable("dbo.Pictures");
            DropTable("dbo.Estates");
            DropTable("dbo.Counterparties");
            DropTable("dbo.Categories");
        }
    }
}
