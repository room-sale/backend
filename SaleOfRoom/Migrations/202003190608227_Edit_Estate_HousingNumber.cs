﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit_Estate_HousingNumber : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Estates", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Estates", "TransactionTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.Estates", "WallMaterialId", "dbo.WallMaterials");
            DropIndex("dbo.Estates", new[] { "CategoryId" });
            DropIndex("dbo.Estates", new[] { "TransactionTypeId" });
            DropIndex("dbo.Estates", new[] { "WallMaterialId" });
            AddColumn("dbo.Estates", "HousingNumber", c => c.Int());
            AlterColumn("dbo.Estates", "Area", c => c.Double());
            AlterColumn("dbo.Estates", "LivingArea", c => c.Double());
            AlterColumn("dbo.Estates", "KitchenArea", c => c.Double());
            AlterColumn("dbo.Estates", "RoomCount", c => c.Int());
            AlterColumn("dbo.Estates", "Price", c => c.Double());
            AlterColumn("dbo.Estates", "PriceArea", c => c.Double());
            AlterColumn("dbo.Estates", "HouseNumber", c => c.Int());
            AlterColumn("dbo.Estates", "CategoryId", c => c.Int());
            AlterColumn("dbo.Estates", "TransactionTypeId", c => c.Int());
            AlterColumn("dbo.Estates", "WallMaterialId", c => c.Int());
            AlterColumn("dbo.Estates", "StoreysNumber", c => c.Int());
            AlterColumn("dbo.Estates", "Commission", c => c.Double());
            CreateIndex("dbo.Estates", "CategoryId");
            CreateIndex("dbo.Estates", "TransactionTypeId");
            CreateIndex("dbo.Estates", "WallMaterialId");
            AddForeignKey("dbo.Estates", "CategoryId", "dbo.Categories", "Id");
            AddForeignKey("dbo.Estates", "TransactionTypeId", "dbo.TransactionTypes", "Id");
            AddForeignKey("dbo.Estates", "WallMaterialId", "dbo.WallMaterials", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Estates", "WallMaterialId", "dbo.WallMaterials");
            DropForeignKey("dbo.Estates", "TransactionTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.Estates", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Estates", new[] { "WallMaterialId" });
            DropIndex("dbo.Estates", new[] { "TransactionTypeId" });
            DropIndex("dbo.Estates", new[] { "CategoryId" });
            AlterColumn("dbo.Estates", "Commission", c => c.Double(nullable: false));
            AlterColumn("dbo.Estates", "StoreysNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.Estates", "WallMaterialId", c => c.Int(nullable: false));
            AlterColumn("dbo.Estates", "TransactionTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Estates", "CategoryId", c => c.Int(nullable: false));
            AlterColumn("dbo.Estates", "HouseNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.Estates", "PriceArea", c => c.Double(nullable: false));
            AlterColumn("dbo.Estates", "Price", c => c.Double(nullable: false));
            AlterColumn("dbo.Estates", "RoomCount", c => c.Int(nullable: false));
            AlterColumn("dbo.Estates", "KitchenArea", c => c.Double(nullable: false));
            AlterColumn("dbo.Estates", "LivingArea", c => c.Double(nullable: false));
            AlterColumn("dbo.Estates", "Area", c => c.Double(nullable: false));
            DropColumn("dbo.Estates", "HousingNumber");
            CreateIndex("dbo.Estates", "WallMaterialId");
            CreateIndex("dbo.Estates", "TransactionTypeId");
            CreateIndex("dbo.Estates", "CategoryId");
            AddForeignKey("dbo.Estates", "WallMaterialId", "dbo.WallMaterials", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Estates", "TransactionTypeId", "dbo.TransactionTypes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Estates", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
    }
}
