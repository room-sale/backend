﻿namespace SaleOfRoom.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using SaleOfRoom.Common;
    using SaleOfRoom.Models.SaleOfRoom;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SaleOfRoom.Common.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SaleOfRoom.Common.ApplicationDbContext context)
        {
            #region Role

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            roleManager.Create(new IdentityRole() { Name = "Admin" });
            roleManager.Create(new IdentityRole() { Name = "Realtor" });

            #endregion

            #region Admin user

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var admin = new ApplicationUser()
            {
                UserName = "Elena",
                Email = "ant.ivanov20@yandex.ru",
                EmailConfirmed = true,
            };
            
            var result = userManager.Create(admin, "P111hh150?");

            if (result.Succeeded)
            {
                userManager.AddToRole(admin.Id, "Admin");
            }

            #endregion.

            #region Services

            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                dbContext.Categories.Add(new Category() { Name = "Квартира" });
                dbContext.Categories.Add(new Category() { Name = "Студия" });
                dbContext.Categories.Add(new Category() { Name = "Комната" });
                dbContext.Categories.Add(new Category() { Name = "Дача" });
                dbContext.Categories.Add(new Category() { Name = "Земельный участок" });
                dbContext.Categories.Add(new Category() { Name = "Гараж и машиноместо" });
                dbContext.Categories.Add(new Category() { Name = "Новостройки" });
                dbContext.Categories.Add(new Category() { Name = "Коммерческое" });

                dbContext.TransactionTypes.Add(new TransactionType() { Name = "Продажа" });
                dbContext.TransactionTypes.Add(new TransactionType() { Name = "Аренда" });

                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Кирпич" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Брус" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Бревно" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Металл" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Сэндвич-панели" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Ж/б панели" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Экспериментальные материалы" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Панельный" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Блочный" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Монолитный" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Деревянный" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Пеноблоки" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Монолитно-кирпичный" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Каркасный" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Газосиликатный блок" });
                dbContext.WallMaterials.Add(new WallMaterial() { Name = "Другой" });

                dbContext.SaveChanges();
            }


            #endregion
        }
    }
}
