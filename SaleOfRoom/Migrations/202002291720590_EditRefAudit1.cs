﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditRefAudit1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EntityLogs", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.EntityLogs", "UserId");
            AddForeignKey("dbo.EntityLogs", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EntityLogs", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.EntityLogs", new[] { "UserId" });
            AlterColumn("dbo.EntityLogs", "UserId", c => c.String());
        }
    }
}
