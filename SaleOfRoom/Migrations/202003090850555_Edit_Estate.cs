﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit_Estate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Estates", "Header", c => c.String());
            AddColumn("dbo.Estates", "Message", c => c.String());
            AddColumn("dbo.Estates", "RommType", c => c.String());
            AddColumn("dbo.Estates", "Status", c => c.String());
            AddColumn("dbo.Estates", "RoomState", c => c.String());
            AddColumn("dbo.Estates", "StoreysNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Estates", "Commission", c => c.Double(nullable: false));
            AddColumn("dbo.Estates", "Repairs", c => c.String());
            AddColumn("dbo.Estates", "CadastralNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Estates", "CadastralNumber");
            DropColumn("dbo.Estates", "Repairs");
            DropColumn("dbo.Estates", "Commission");
            DropColumn("dbo.Estates", "StoreysNumber");
            DropColumn("dbo.Estates", "RoomState");
            DropColumn("dbo.Estates", "Status");
            DropColumn("dbo.Estates", "RommType");
            DropColumn("dbo.Estates", "Message");
            DropColumn("dbo.Estates", "Header");
        }
    }
}
