﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditEstateRealtorFK : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Estates", name: "Realtor_UserId", newName: "UserId");
            RenameIndex(table: "dbo.Estates", name: "IX_Realtor_UserId", newName: "IX_UserId");
            DropColumn("dbo.Estates", "RealtorId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Estates", "RealtorId", c => c.String());
            RenameIndex(table: "dbo.Estates", name: "IX_UserId", newName: "IX_Realtor_UserId");
            RenameColumn(table: "dbo.Estates", name: "UserId", newName: "Realtor_UserId");
        }
    }
}
