﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit_Estate_FieldsForExport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Estates", "DealStatus", c => c.String());
            AddColumn("dbo.Estates", "OnDomClick", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Estates", "OnDomClick");
            DropColumn("dbo.Estates", "DealStatus");
        }
    }
}
