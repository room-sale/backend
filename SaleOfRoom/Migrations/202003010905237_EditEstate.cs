﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditEstate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Estates", "PriceArea", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Estates", "PriceArea");
        }
    }
}
