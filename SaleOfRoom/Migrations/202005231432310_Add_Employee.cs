﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Employee : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pictures", "EstateId", "dbo.Estates");
            DropIndex("dbo.Pictures", new[] { "EstateId" });
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        LastName = c.String(),
                        Position = c.String(),
                        PictureId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pictures", t => t.PictureId)
                .Index(t => t.PictureId);
            
            AlterColumn("dbo.Pictures", "EstateId", c => c.Guid());
            CreateIndex("dbo.Pictures", "EstateId");
            AddForeignKey("dbo.Pictures", "EstateId", "dbo.Estates", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pictures", "EstateId", "dbo.Estates");
            DropForeignKey("dbo.Employees", "PictureId", "dbo.Pictures");
            DropIndex("dbo.Employees", new[] { "PictureId" });
            DropIndex("dbo.Pictures", new[] { "EstateId" });
            AlterColumn("dbo.Pictures", "EstateId", c => c.Guid(nullable: false));
            DropTable("dbo.Employees");
            CreateIndex("dbo.Pictures", "EstateId");
            AddForeignKey("dbo.Pictures", "EstateId", "dbo.Estates", "Id", cascadeDelete: true);
        }
    }
}
