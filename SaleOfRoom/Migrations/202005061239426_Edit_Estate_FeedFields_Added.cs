﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit_Estate_FeedFields_Added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Estates", "OnRestat", c => c.Boolean());
            AddColumn("dbo.Estates", "OnMirKvartir", c => c.Boolean());
            AddColumn("dbo.Estates", "OnGdeEtotDom", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Estates", "OnGdeEtotDom");
            DropColumn("dbo.Estates", "OnMirKvartir");
            DropColumn("dbo.Estates", "OnRestat");
        }
    }
}
