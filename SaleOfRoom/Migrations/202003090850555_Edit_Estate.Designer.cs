﻿// <auto-generated />
namespace SaleOfRoom.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class Edit_Estate : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Edit_Estate));
        
        string IMigrationMetadata.Id
        {
            get { return "202003090850555_Edit_Estate"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
