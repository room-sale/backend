﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditRefAudit : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EntityLogs", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.EntityLogs", new[] { "UserId" });
            AlterColumn("dbo.EntityLogs", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EntityLogs", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.EntityLogs", "UserId");
            AddForeignKey("dbo.EntityLogs", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
