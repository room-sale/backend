﻿namespace SaleOfRoom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUsers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "isDisabled", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "isDisabled");
        }
    }
}
