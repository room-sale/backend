﻿using SaleOfRoom.Models.SaleOfRoom;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace SaleOfRoom.Common.Converters
{
    public static class EstateConverter
    {
        public static string GetEstateYRL(Estate[] estates)
        {
            ContainerEstateYRL container = new ContainerEstateYRL();

            foreach (var estate in estates)
                container.Offer.Add(estate.ToResidentialEstateYRL());

            using (XParse parse = new XParse())
            {
                return parse.Serialize(container);
                //Stream stream = GenerateStreamFromString(parse.Serialize(container));

                //return new FileStreamResult(stream, "text/xml");
            }
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }

    public class XParse : IDisposable
    {
        private StringBuilder Result { get; set; }

        public void Dispose() => Result.Clear();

        public string Serialize(object obj)
        {
            Result = new StringBuilder(@"<?xml version=""1.0"" encoding=""utf-8""?>");
#if DEBUG
            Result.Append("\n");
#endif
            Type type = obj.GetType();
            XRoot root = type.GetCustomAttribute<XRoot>();

            if (root == null)
                throw new ArgumentNullException();

            Result.Append($"<{root.Name}");

            if (!string.IsNullOrEmpty(root.Namespace))
                Result.Append($@" xmlns=""{root.Namespace}""");
            Result.Append(">");
#if DEBUG
            Result.Append("\n");
#endif
            BuildValue(obj);
            Result.Append($"</{root.Name}>");

            return Result.ToString();
        }

        private void BuildValue(object obj)
        {
            Type type = obj.GetType();

            if (type.IsClass)
            {
                foreach (PropertyInfo property in obj.GetType().GetProperties())
                {
                    if (property.GetCustomAttribute<XIgnore>() != null || property.GetCustomAttribute<XAttribute>() != null)
                        continue;

                    XName name = property.GetCustomAttribute<XName>();
                    XList list = property.GetCustomAttribute<XList>();

                    object value = property.GetValue(obj);

                    if (value == null)
                        continue;

                    if (list != null)
                        foreach (object element in (value as IEnumerable))
                        {
                            Type elementType = element.GetType();

                            if (elementType != typeof(string) && elementType.IsClass)
                                AddClass(list.Name, element);
                            else
                                AddValue(list.Name, element.ToString());
                        }
                    else if (name != null)
                    {
                        if (property.PropertyType != typeof(string) && property.PropertyType.IsClass)
                            AddClass(name.Name, value);
                        else
                            AddValue(name.Name, value.ToString());
                    }
                }
            }
            else
                throw new ArgumentNullException();
        }

        private class Attribute
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }

        private void AddClass(string name, object obj)
        {
            Type type = obj.GetType();
            StartClass(
                name,
                type
                    .GetProperties()
                    .Where(p => p.GetCustomAttribute<XAttribute>() != null)
                    .Select(p => new Attribute
                    {
                        Name = p.GetCustomAttribute<XAttribute>().Name,
                        Value = p.GetValue(obj).ToString()
                    })
                    .ToList()
                );
            BuildValue(obj);
            EndClass(name);
        }

        private void AddValue(string name, string value)
        {
            Result.Append($"<{name}>{GetString(value)}</{name}>");
#if DEBUG
            Result.Append("\n");
#endif
        }

        private void StartClass(string name, List<Attribute> attributes = null)
        {
            Result.Append($"<{name}");

            if (attributes?.Count > 0)
            {
                foreach (Attribute attribute in attributes)
                    Result.Append($@" {attribute.Name}=""{GetString(attribute.Value)}""");
            }

            Result.Append($">");
#if DEBUG
            Result.Append("\n");
#endif
        }

        private void EndClass(string name)
        {
            Result.Append($"</{name}>");
#if DEBUG
            Result.Append("\n");
#endif
        }

        private string GetString(string value)
            => value
                .Replace(@"""", "&quot;")
                .Replace(@"&", " &amp;")
                .Replace(@">", "&gt;")
                .Replace(@"<", "&lt;")
                .Replace(@"'", "&apos;");
    }
}