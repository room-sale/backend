﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace SaleOfRoom.Common.Converters
{
    public class XName : Attribute
    {
        public string Name { get; set; }

        public XName(string name)
            => Name = name;
    }

    public class XRoot : XName
    {
        public string Namespace { get; set; }

        public XRoot(string name, string @namespace) : base(name)
            => Namespace = @namespace;
    }

    public class XAttribute : XName
    {
        public XAttribute(string name) : base(name) { }
    }

    public class XList : XName
    {
        public XList(string name) : base(name) { }
    }

    public class XIgnore : Attribute { }

    [XRoot("realty-feed", "http://webmaster.yandex.ru/schemas/feed/realty/2010-06")]
    public class ContainerEstateYRL
    {
        [XList("offer")]
        public List<ResidentialEstateYRL> Offer { get; set; }

        public ContainerEstateYRL()
        {
            Offer = new List<ResidentialEstateYRL>();
        }
    }

    public class ResidentialEstateYRL
    {
        /// <summary>
        /// Уникальный id
        /// </summary>
        [XAttribute("internal-id")]
        public string InternalId { get; set; }

        /// <summary>
        /// Тип сделки. Строго ограниченные значения: «продажа» «аренда».
        /// </summary>
        [XName("type")]
        public string Type { get; set; }
        /// <summary>
        /// Тип недвижимости.Строго ограниченное значение: «жилая»/«living».
        /// </summary>
        [XName("property-type")]
        public virtual string PropertyType { get; set; } = "жилая";
        /// <summary>
        /// Категория объекта. Возможные значения: «дача»/«коттедж»/«cottage» «дом»/«house»
        /// </summary>
        [XName("category")]
        public string Category { get; set; }
        /// <summary>
        /// Категория гаража. Обязательный элемент для объявлений о продаже и аренде гаражей. Возможные значения: «гараж»/«garage» «машиноместо»/«parking place» «бокс»/«box»
        /// </summary>
        [XName("garage-type")]
        public string GarageType { get; set; }
        /// <summary>
        /// Кадастровый номер объекта недвижимости.
        /// </summary>
        [XName("cadastral-number")]
        public string CadastralNumber { get; set; }

        /// <summary>
        /// Дата создания объявления.
        /// </summary>
        [XIgnore]
        public DateTime CreationDate { get; set; }
        [XName("creation-date")]
        public string CreationDateString
        {
            get => CreationDate.ToString("yyyy-MM-ddTHH:mm:ss+03:00");
            set => CreationDate = DateTime.Parse(value);
        }


        public class EstateLocation
        {
            /// <summary>
            /// Страна, в которой расположен объект.
            /// </summary>
            [XName("country")]
            [Required]
            public string Country { get; set; } = "Россия";
            /// <summary>
            /// Название субъекта РФ.
            /// </summary>
            [XName("region")]
            public string Region { get; set; }
            /// <summary>
            /// Название района субъекта РФ.
            /// </summary>
            [XName("district")]
            public string District { get; set; }
            /// <summary>
            /// Название населенного пункта.
            /// </summary>
            [XName("locality-name")]
            public string LocalityName { get; set; }
            /// <summary>
            /// Район в населенном пункте.
            /// </summary>
            [XName("sub-locality-name")]
            public string SubLocalityName { get; set; }
            /// <summary>
            /// Адрес объекта — улица и номер здания.
            /// </summary>
            [XName("address")]
            public string Address { get; set; }
            /// <summary>
            /// Номер квартиры.
            /// </summary>
            [XName("apartment")]
            public string Apartment { get; set; }
            /// <summary>
            /// Шоссе
            /// </summary>
            [XName("direction")]
            public string Direction { get; set; }
            /// <summary>
            /// Расстояние по шоссе до МКАД.
            /// </summary>
            [XName("distance")]
            public string Distance { get; set; }
            /// <summary>
            /// Географическая широта.
            /// </summary>
            [XName("latitude")]
            public double Latitude { get; set; }
            /// <summary>
            /// Географическая долгота.
            /// </summary>
            [XName("longitude")]
            public double Longitude { get; set; }
            /// <summary>
            /// Ближайшая железнодорожная станция.
            /// </summary>
            [XName("railway-station")]
            public string RailwayStation { get; set; }

            public class LocationMetro
            {
                /// <summary>
                /// Ближайшая станция метро.
                /// </summary>
                [XName("name")]
                public string Name { get; set; }
                /// <summary>
                /// Время до метро в минутах на транспорте.
                /// </summary>
                [XName("time-on-transport")]
                public int TimeOnTransport { get; set; }
                /// <summary>
                /// Время до метро в минутах пешком.
                /// </summary>
                [XName("time-on-foot")]
                public int TimeOnFoot { get; set; }
            }
            [XName("metro")]
            public LocationMetro Metro { get; set; }

            /// <summary>
            /// Название коттеджного посёлка.
            /// </summary>
            [XName("village-name")]
            public string VillageName { get; set; }
            /// <summary>
            /// Идентификатор коттеджного поселка в базе данных Яндекса.
            /// </summary>
            [XName("yandex-village-id")]
            public string YandexVillageId { get; set; }
        }
        [XName("location")]
        public EstateLocation Location { get; set; }

        public class EstateSalesAgent
        {
            /// <summary>
            /// Имя продавца, арендодателя или агента.
            /// </summary>
            [XName("name")]
            public string Name { get; set; }
            /// <summary>
            /// Номер телефона.
            /// </summary>
            [XName("phone")]
            public string Phone { get; set; }
            /// <summary>
            /// Тип продавца или арендодателя.
            /// </summary>
            [XName("category")]
            public string Category { get; set; }
            /// <summary>
            /// Название организации.
            /// </summary>
            [XName("organization")]
            public string Organization { get; set; }
            /// <summary>
            /// Сайт агентства или застройщика.
            /// </summary>
            [XName("url")]
            public string Url { get; set; }
            /// <summary>
            /// Электронный адрес продавца. Мы не покажем его покупателям.
            /// </summary>
            [XName("email")]
            public string Email { get; set; }
            /// <summary>
            /// Ссылка на фотографию агента или логотип компании.
            /// </summary>
            [XName("photo")]
            public string Photo { get; set; }
        }
        [XName("sales-agent")]
        public EstateSalesAgent SalesAgent { get; set; }

        public class EstatePrice
        {
            /// <summary>
            /// Цена.
            /// </summary>
            [XName("value")]
            public double Value { get; set; }
            // RUB | EUR | USD
            [XName("currency")]
            public string Currency { get; set; } = "RUB";
            /// <summary>
            /// Период для расчета стоимости аренды. Обязательный для аренды.Не указывайте для продажи. "день" | "месяц"
            /// </summary>
            [XName("period")]
            public string Period { get; set; }
            /// <summary>
            /// Единица площади помещения или участка. "кв. м" | "cотка" | "гектар"
            /// </summary>
            [XName("unit")]
            public string Unit { get; set; }
            /// <summary>
            /// Залог. + | -
            /// </summary>
            [XName("rent-pledge")]
            public string RentPledge { get; set; }
        }
        [XName("price")]
        public EstatePrice Price { get; set; }

        /// <summary>
        /// Тип сделки.
        /// Если элемент отсутствует, все объявления партнера в новостройках считаются квартирами от застройщика.
        /// Возможные значения:
        ///     «первичная продажа»/«продажа от застройщика»
        ///     «переуступка»/«reassignment».
        /// Возможные значения для вторичной недвижимости:
        ///     «прямая продажа»/«sale»
        ///     «первичная продажа вторички»/«primary sale of secondary»
        ///     «встречная продажа»/«countersale».
        /// </summary>
        [XName("deal-status")]
        public string DealStatus { get; set; }
        /// <summary>
        /// Торг. + | -
        /// </summary>
        [XName("haggle")]
        public string Haggle { get; set; }
        /// <summary>
        /// Ипотека. + | -
        /// </summary>
        [XName("mortgage")]
        public string Mortgage { get; set; }
        /// <summary>
        /// Предоплата (%)
        /// </summary>
        [XName("prepayment")]
        public double Prepayment { get; set; }
        /// <summary>
        /// Комиссия агента. (%)
        /// </summary>
        [XName("agent-fee")]
        public double AgentFee { get; set; }
        /// <summary>
        /// Пометка «Просьба агентам не звонить». + | -
        /// </summary>
        [XName("not-for-agents")]
        public string NotForAgents { get; set; }
        /// <summary>
        /// Коммунальные услуги включены в стоимость в договоре аренды. + | -
        /// </summary>
        [XName("utilities-included")]
        public string UtilitiesIncluded { get; set; }

        public class EstateArea
        {
            /// <summary>
            /// Площадь.
            /// </summary>
            [XName("value")]
            public double Value { get; set; }
            // кв. м | cотка | гектар
            /// <summary>
            /// Единица площади помещения или участка. кв. м | cотка | гектар
            /// </summary>
            [XName("unit")]
            public string Unit { get; set; }
        }
        /// <summary>
        /// Общая площадь.
        /// </summary>
        [XName("area")]
        public EstateArea Area { get; set; }
        /// <summary>
        /// Площадь комнаты. Обязательный элемент для продажи и аренды комнаты.
        /// </summary>
        [XList("room-space")]
        public List<EstateArea> RoomSpace { get; set; }
        /// <summary>
        /// Жилая площадь.
        /// </summary>
        [XName("living-space")]
        public EstateArea LivingSpace { get; set; }
        /// <summary>
        /// Площадь кухни.
        /// </summary>
        [XName("kitchen-space")]
        public EstateArea KitchenSpace { get; set; }

        public class EstateLotArea : EstateArea
        {
            /// <summary>
            /// Тип участка. Возможные значения: «ИЖС» «садоводство»
            /// </summary>
            [XName("lot-type")]
            public string LotType { get; set; }
        }
        /// <summary>
        /// Площадь участка. Указывается для категорий: «дом с участком» и «участок».
        /// </summary>
        [XName("lot-area")]
        public EstateArea LotArea { get; set; }
        /// <summary>
        /// Фотография. элемент.Добавьте больше 4 фото.
        /// </summary>
        [XList("image")]
        public List<string> Image { get; set; }
        /// <summary>
        /// Ремонт. (евроремонт, косметический, дизайнерский, требует ремонта)
        /// </summary>
        [XName("renovation")]
        public string Renovation { get; set; }
        /// <summary>
        /// Состояние объекта. (отличное, хорошее, нормальное, плохое)
        /// </summary>
        [XName("quality")]
        public string Quality { get; set; }
        /// <summary>
        /// Дополнительная информация.
        /// </summary>
        [XName("description")]
        public string Description { get; set; }
        /// <summary>
        /// Убрать из объявления типовую планировку квартиры.
        /// Может быть только true, иначе не указывать
        /// </summary>
        [XName("disable-flat-plan-guess")]
        public string DisableFlatPlanGuess { get; set; }
        /// <summary>
        /// Общее количество комнат в квартире.
        /// При свободной планировке укажите количество комнат по паспорту объекта.
        /// Не обязательно для частных домов.
        /// Не указывайте для студий.
        /// </summary>
        [XName("rooms")]
        public int Rooms { get; set; }
        /// <summary>
        /// Количество комнат, участвующих в сделке.
        /// Указывайте только при продаже или сдаче комнаты.
        /// Не используйте для студий и квартир со свободной планировкой.
        /// </summary>
        [XName("rooms-offered")]
        public int RoomsOffered { get; set; }
        /// <summary>
        /// Этаж, на котором находится объект.
        /// Используйте при продаже или сдаче квартиры и комнаты.
        /// </summary>
        [XName("floor")]
        public int Floor { get; set; }
        /// <summary>
        /// Апартаменты.
        /// </summary>
        [XName("apartments")]
        public bool? Apartments { get; set; }
        /// <summary>
        /// Студия. Элемент используется только для объявлений о продаже и аренде квартиры.
        /// Может быть только true, иначе не указывать
        /// </summary>
        [XName("studio")]
        public bool? Studio { get; set; }
        /// <summary>
        /// Свободная планировка. Элемент используется только для объявлений о продаже и аренде квартиры.
        /// Может быть только true, иначе не указывать
        /// </summary>
        [XName("open-plan")]
        public bool? OpenPlan { get; set; }
        /// <summary>
        /// Тип комнат. (смежные, раздельные)
        /// </summary>
        [XName("rooms-type")]
        public string RoomsType { get; set; }
        /// <summary>
        /// Вид из окон. (во двор, на улицу)
        /// </summary>
        [XName("window-view")]
        public string WindowView { get; set; }
        /// <summary>
        /// Тип балкона. (балкон, лоджия, 2 балкона, 2 лоджии....)
        /// </summary>
        [XName("balcony")]
        public string Balcony { get; set; }
        /// <summary>
        /// Тип санузла. (совмещенный, раздельный, числовое значение (например «2»).)
        /// </summary>
        [XName("bathroom-unit")]
        public string BathroomUnit { get; set; }
        /// <summary>
        /// Наличие системы кондиционирования.
        /// </summary>
        [XName("air-conditioner")]
        public bool? AirConditioner { get; set; }
        /// <summary>
        /// Наличие телефона.
        /// </summary>
        [XName("phone")]
        public bool? Phone { get; set; }
        /// <summary>
        /// Наличие интернета.
        /// </summary>
        [XName("internet")]
        public bool? Internet { get; set; }
        /// <summary>
        /// Наличие мебелли.
        /// </summary>
        [XName("room-furniture")]
        public bool? RoomFurniture { get; set; }
        /// <summary>
        /// Наличие мебели на кухне.
        /// </summary>
        [XName("kitchen-furniture")]
        public bool? KitchenFurniture { get; set; }
        /// <summary>
        /// Наличие телевизора.
        /// </summary>
        [XName("television")]
        public bool? Еelevision { get; set; }
        /// <summary>
        /// Наличие стиральной машины.
        /// </summary>
        [XName("washing-machine")]
        public bool? WashingMachine { get; set; }
        /// <summary>
        /// Наличие системы кондиционирования.
        /// </summary>
        [XName("dishwasher")]
        public bool? Dishwasher { get; set; }
        /// <summary>
        /// Наличие холодильника.
        /// </summary>
        [XName("refrigerator")]
        public bool? Refrigerator { get; set; }
        /// <summary>
        /// Встроенная техника.
        /// </summary>
        [XName("built-in-tech")]
        public bool? BuiltInTech { get; set; }
        /// <summary>
        /// Покрытие пола. (ковролин, ламинат, линолеум, паркет)
        /// </summary>
        [XName("floor-covering")]
        public bool? FloorCovering { get; set; }
        /// <summary>
        /// Проживание с детьми.
        /// </summary>
        [XName("with-children")]
        public bool? WithChildren { get; set; }
        /// <summary>
        /// Проживание с животными.
        /// </summary>
        [XName("with-pets")]
        public bool? WithPets { get; set; }
        /// <summary>
        /// Общее количество этажей в доме.
        /// </summary>
        [XName("floors-total")]
        public int? FloorsTotal { get; set; }
        /// <summary>
        /// Название жилого комплекса.
        /// </summary>
        [XName("building-name")]
        public string BuildingName { get; set; }
        /// <summary>
        /// Идентификатор жилого комплекса в базе Яндекса.
        /// Нужен чтобы объявления соответствовали правильному корпусу в ЖК.
        /// Указан в третьем столбце https://realty.yandex.ru/newbuildings.tsv
        /// </summary>
        [XName("yandex-building-id")]
        public string YandexBuildingId { get; set; }
        /// <summary>
        /// Идентификатор корпуса жилого комплекса в базе Яндекса.
        /// Нужен чтобы объявления соответствовали правильному корпусу в ЖК.
        /// Указан в седьмом столбце https://realty.yandex.ru/newbuildings.tsv
        /// </summary>
        [XName("yandex-house-id")]
        public string YandexHouseId { get; set; }
        /// <summary>
        /// Год сдачи (год постройки).
        /// Обязательный элемент для домов (жилищных комплексов), которые были сданы менее 5 лет назад или будут сданы в будущем.
        /// Год необходимо указывать полностью, например — «1996», а не «96».
        /// Необязательный элемент для частных домов.
        /// </summary>
        [XName("built-year")]
        public int? BuiltYear { get; set; }
        /// <summary>
        /// Корпус дома. Возможные значения: «корпус 1», «корпус А», «дом 3» и т. п.
        /// </summary>
        [XName("building-section")]
        public string BuildingSection { get; set; }
        /// <summary>
        /// Высота потолков в метрах.
        /// </summary>
        [XName("ceiling-height")]
        public double? CeilingHeight { get; set; }
        /// <summary>
        /// Закрытая территория.
        /// </summary>
        [XName("guarded-building")]
        public bool? GuardedBuilding { get; set; }
        /// <summary>
        /// Возможность ПМЖ.
        /// </summary>
        [XName("pmg")]
        public bool? PMG { get; set; }
        /// <summary>
        /// Наличие пропускной системы.
        /// </summary>
        [XName("access-control-system")]
        public bool? AccessControlSystem { get; set; }
        /// <summary>
        /// Лифт.
        /// </summary>
        [XName("lift")]
        public bool? Lift { get; set; }
        /// <summary>
        /// Мусоропровод.
        /// </summary>
        [XName("rubbish-chute")]
        public bool? RubbishChute { get; set; }
        /// <summary>
        /// Электричество. Элемент используется для домов.
        /// </summary>
        [XName("electricity-supply")]
        public bool? ElectricitySupply { get; set; }
        /// <summary>
        /// Водопровод. Элемент используется для домов.
        /// </summary>
        [XName("water-supply")]
        public bool? WaterSupply { get; set; }
        /// <summary>
        /// Газ. Элемент используется для домов.
        /// </summary>
        [XName("gas-supply")]
        public bool? GasSupply { get; set; }
        /// <summary>
        /// Канализация. Элемент используется для домов.
        /// </summary>
        [XName("sewerage-supply")]
        public bool? SewerageSupply { get; set; }
        /// <summary>
        /// Отопление. Элемент используется для домов.
        /// </summary>
        [XName("heating-supply")]
        public bool? HeatingSupply { get; set; }
        /// <summary>
        /// Туалет. Элемент используется для домов.
        /// </summary>
        [XName("toilet")]
        public bool? Toilet { get; set; }
        /// <summary>
        /// Душ. Элемент используется для домов.
        /// </summary>
        [XName("shower")]
        public bool? Shower { get; set; }
        /// <summary>
        /// Бассейн. Элемент используется для домов.
        /// </summary>
        [XName("pool")]
        public bool? Pool { get; set; }
        /// <summary>
        /// Бильярд. Элемент используется для домов.
        /// </summary>
        [XName("billiard")]
        public bool? Billiard { get; set; }
        /// <summary>
        /// Сауна. Элемент используется для домов.
        /// </summary>
        [XName("sauna")]
        public bool? Sauna { get; set; }
        /// <summary>
        /// Наличие охраняемой парковки.
        /// </summary>
        [XName("parking")]
        public bool? Parking { get; set; }
        /// <summary>
        /// Количество предоставляемых парковочных мест.
        /// </summary>
        [XName("parking-places")]
        public int? ParkingPlaces { get; set; }
        /// <summary>
        /// Стоимость парковочного места. Указывается стоимость одного места в месяц в рублях.
        /// </summary>
        [XName("parking-place-price")]
        public double? ParkingPlacePrice { get; set; }
        /// <summary>
        /// Наличие гостевых парковочных мест.
        /// </summary>
        [XName("parking-guest")]
        public bool? ParkingGuest { get; set; }
        /// <summary>
        /// Наличие сигнализации в доме.
        /// </summary>
        [XName("alarm")]
        public bool? Alarm { get; set; }
        /// <summary>
        /// Наличие сигнализации в квартире.
        /// </summary>
        [XName("flat-alarm")]
        public bool? FlatAlarm { get; set; }
        /// <summary>
        /// Наличие охраны.
        /// </summary>
        [XName("security")]
        public bool? Security { get; set; }
        /// <summary>
        /// Элитная недвижимость.
        /// </summary>
        [XName("is-elite")]
        public bool? IsElite { get; set; }
        /// <summary>
        /// Статус собственности. («собственность»/«private», «кооператив»/«cooperative», «по доверенности»/«by proxy»)
        /// </summary>
        [XName("ownership-type")]
        public string OwnershipType { get; set; }
        /// <summary>
        /// Название гаражно-строительного кооператива.
        /// </summary>
        [XName("garage-name")]
        public string GarageName { get; set; }
        /// <summary>
        /// Тип парковки. Элемент используется только для бокса и парковочного места. («подземная»/«underground», «наземная»/«ground», «многоуровневая»/«multilevel»)
        /// </summary>
        [XName("parking-type")]
        public string ParkingType { get; set; }
        /// <summary>
        /// Материал стен. Элемент используется только для гаража. «кирпичный»/«brick» «железобетонный»/«ferroconcrete» «металлический»/«metal».
        /// </summary>
        [XName("building-type")]
        public string BuildingType { get; set; }
        /// <summary>
        /// Наличие автоматических ворот.
        /// </summary>
        [XName("automatic-gates")]
        public bool? AutomaticGates { get; set; }
        /// <summary>
        /// Наличие видеонаблюдения.
        /// </summary>
        [XName("cctv")]
        public bool? CCTV { get; set; }
        /// <summary>
        /// Наличие пожарной сигнализации.
        /// </summary>
        [XName("fire-alarm")]
        public bool? FireAlarm { get; set; }
        /// <summary>
        /// Наличие смотровой ямы.
        /// </summary>
        [XName("inspection-pit")]
        public bool? InspectionPit { get; set; }
        /// <summary>
        /// Наличие подвала или погреба.
        /// </summary>
        [XName("cellar")]
        public bool? Cellar { get; set; }
        /// <summary>
        /// Наличие автомойки.
        /// </summary>
        [XName("car-wash")]
        public bool? CarWash { get; set; }
        /// <summary>
        /// Наличие автосервиса.
        /// </summary>
        [XName("auto-repair")]
        public bool? AutoRepair { get; set; }
        /// <summary>
        /// Признак гаража в новостройке.
        /// </summary>
        [XName("new-parking")]
        public bool? NewParking { get; set; }
    }
}