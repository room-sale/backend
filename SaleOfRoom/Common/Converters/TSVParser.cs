﻿using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SaleOfRoom.Common.Converters
{
    public static class TSVParser
    {
        public static List<string[]> ParseFromUrl(string url)
        {
            WebClient web = new WebClient();
            string tsv = Encoding.UTF8.GetString(web.DownloadData(url));

            return Parse(tsv);
        }

        public static List<string[]> Parse(string text)
        {
            List<string[]> result = new List<string[]>();

            foreach (string line in text.Split('\n'))
                result.Add(line.Split('\t'));

            return result;
        }

        public static List<RealtyYandex> GetRealtyYandex()
        {
            List<RealtyYandex> result = new List<RealtyYandex>();

            foreach (string[] data in ParseFromUrl("https://realty.yandex.ru/newbuildings.tsv"))
            {
                try
                {
                    result.Add(new RealtyYandex
                    {
                        Region = data[0],
                        District = data[1],
                        Id = int.Parse(data[2]),
                        Turn = data[3],
                        Quarter = data[4],
                        Address = data[5],
                        Id2 = int.Parse(data[6]),
                        Url = data[7]
                    });
                }
                catch 
                { 
                }
            }

            return result;
        }
    }

    public class RealtyYandex
    {
        public int Id { get; set; }

        public string Region { get; set; }
        public string District { get; set; }
        public string Turn { get; set; }
        public string Quarter { get; set; }
        public string Address { get; set; }

        public int Id2 { get; set; }
        public string Url { get; set; }
    }
}