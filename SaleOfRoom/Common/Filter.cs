﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Common
{
    public class Filter
    {
        public int? CategoryId { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string CityDistrict { get; set;}
        public string Street { get; set; }
        public int? TransactionTypeId { get; set; }
        public double? PriceFrom { get; set; }
        public double? PriceTo { get; set; }
        public double? AreaFrom { get; set; }
        public double? AreaTo { get; set; }
        public int? WallMaterialId { get; set; }
        public int? RoomCount { get; set; }
    }
}