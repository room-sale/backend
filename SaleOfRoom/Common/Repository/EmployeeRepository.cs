﻿using SaleOfRoom.Common.Repository.Base;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Common.Repository
{
    public class EmployeeRepository : RepositoryBase<Employee>
    {
        public EmployeeRepository(ApplicationDbContext context)
            : base(context)
        { }

        public override void Create(Employee item)
        {
            db.Employees.Add(item);
        }

        public override void Delete(Guid id)
        {
            Employee employee = db.Employees.FirstOrDefault(r => r.Id == id);
            if (employee != null)
                db.Employees.Remove(employee);

        }

        public override Employee Get(Guid id)
        {
            return db.Employees
                        .FirstOrDefault(r => r.Id == id);

        }

        public override IEnumerable<Employee> GetAll()
        {
            return db.Employees;
        }

        public override void Update(Employee item)
        {
            db.SetModified(item);
        }
    }
}