﻿using SaleOfRoom.Common.Repository.Base;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SaleOfRoom.Common.Repository
{
    public class RealtorRepository : RepositoryIdentityBase<Realtor>
    {
        public RealtorRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        public override void Create(Realtor item)
        {
            db.Realtors.Add(item);
        }

        public override void Delete(string id)
        {
            Realtor realtor = db.Realtors.FirstOrDefault(r => r.UserId == id);
            if (realtor != null)
                realtor.isDisabled = !realtor.isDisabled;

        }

        public override Realtor Get(string id)
        {
            return db.Realtors
                        .Include(r => r.User)
                        .Include(r => r.Estates)
                        .FirstOrDefault(r => r.UserId == id);

        }

        public override IEnumerable<Realtor> GetAll()
        {
            return db.Realtors;
        }

        public override void Update(Realtor item)
        {
            db.SetModified(item);
        }
    }
}