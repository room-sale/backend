﻿using SaleOfRoom.Common.Repository.Base;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SaleOfRoom.Common.Repository
{
    public class PictureRepository : RepositoryBase<Picture>
    {
        public PictureRepository(ApplicationDbContext context)
            : base(context)
        {
        }

        public override void Create(Picture item)
        {
            db.Pictures.Add(item);
        }

        public override void Delete(Guid id)
        {
            Picture realtor = db.Pictures.FirstOrDefault(r => r.Id == id);
            if (realtor != null)
                db.Pictures.Remove(realtor);

        }

        public override Picture Get(Guid id)
        {
            return db.Pictures
                        .FirstOrDefault(r => r.Id == id);

        }

        public override IEnumerable<Picture> GetAll()
        {
            return db.Pictures;
        }

        public override void Update(Picture item)
        {
            db.SetModified(item);
        }
    }
}