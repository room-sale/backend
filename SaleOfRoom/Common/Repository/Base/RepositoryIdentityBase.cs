﻿using SaleOfRoom.Common.Repository.Base.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleOfRoom.Common.Repository.Base
{
    public abstract class RepositoryIdentityBase<T> : IIdentityUserRepository<T>, IIdentityRepository<T>
        where T : class
    {
        protected ApplicationDbContext db;

        public RepositoryIdentityBase(ApplicationDbContext context)
        {
            db = context;
        }

        public abstract void Create(T item);

        public abstract void Delete(string id);

        public abstract T Get(string id);

        public abstract IEnumerable<T> GetAll();

        public abstract void Update(T item);

        public IEnumerable<T> ToPagedList(IEnumerable<T> items, int pageNumber, int pageSize)
            => items
                .Skip(pageSize * (pageNumber - 1)).Take(pageSize);
    }

}
