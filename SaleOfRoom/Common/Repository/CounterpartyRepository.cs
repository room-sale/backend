﻿using SaleOfRoom.Common.Repository.Base;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace SaleOfRoom.Common.Repository
{
    public class CounterpartyRepository : RepositoryBase<Counterparty>
    {
        public CounterpartyRepository(ApplicationDbContext db)
            : base(db)
        {
        }

        public override void Create(Counterparty item)
        {
            db.Counterparties.Add(item);
        }

        public override void Delete(Guid id)
        {
            Counterparty counterparty = db.Counterparties.FirstOrDefault(r => r.Id == id);
            if (counterparty != null)
                counterparty.isDisabled = !counterparty.isDisabled;
        }

        public override Counterparty Get(Guid id)
        {
            return db.Counterparties
                        .Include(c => c.Estates)
                        .FirstOrDefault(c => c.Id == id);
        }

        public override IEnumerable<Counterparty> GetAll()
        {
            return db.Counterparties
                        .Include(c => c.Estates);
        }

        public override void Update(Counterparty item)
        {
            db.SetModified(item);
        }
    }
}