﻿using SaleOfRoom.Common.Repository.Base.Interface;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Common.Repository
{
    public class CategoryRepository 
    {
        private ApplicationDbContext db;

        public CategoryRepository(ApplicationDbContext db)
        {
            this.db = db;
        }

        public Category Get(int id)
        {
            return db.Categories.FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<Category> GetAll()
        {
            return db.Categories;
        }
    }

    public class TransactionTypeRepository
    {
        private ApplicationDbContext db;

        public TransactionTypeRepository(ApplicationDbContext db)
        {
            this.db = db;
        }

        public TransactionType Get(int id)
        {
            return db.TransactionTypes.FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<TransactionType> GetAll()
        {
            return db.TransactionTypes;
        }
    }

    public class WallMaterialRepository
    {
        private ApplicationDbContext db;

        public WallMaterialRepository(ApplicationDbContext db)
        {
            this.db = db;
        }

        public WallMaterial Get(int id)
        {
            return db.WallMaterials.FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<WallMaterial> GetAll()
        {
            return db.WallMaterials;
        }
    }

    public class HistoryRepository
    {
        private ApplicationDbContext db;

        public HistoryRepository(ApplicationDbContext db)
        {
            this.db = db;
        }

        public EntityLog Get(Guid id)
        {
            return db.EntityLogs.FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<EntityLog> GetAll(string tableName)
        {
            return db.EntityLogs.Where(e => e.TableName == tableName);
        }

        public IEnumerable<EntityLog> ToPagedList(IEnumerable<EntityLog> items, int pageNumber, int pageSize)
            => items
                .Skip(pageSize * (pageNumber - 1)).Take(pageSize);
    }
}