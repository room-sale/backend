﻿using SaleOfRoom.Common.Repository.Base;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SaleOfRoom.Common.Repository.Base.Interface;

namespace SaleOfRoom.Common.Repository
{
    public class EstateRepository : RepositoryBase<Estate>, IUserRepository<Estate>
    {
        public EstateRepository(ApplicationDbContext db) 
            : base(db)
        {
        }

        public override void Create(Estate item)
        {
            db.Estates.Add(item);
        }

        public override void Delete(Guid id)
        {
            Estate estate = db.Estates.FirstOrDefault(r => r.Id == id);
            if (estate != null)
                estate.isDisabled = !estate.isDisabled;
        }

        public override Estate Get(Guid id)
        {
            return db.Estates
                        .Include(r => r.Counterparty)
                        .Include(r => r.Realtor)
                        .Include(r => r.Pictures)
                        .Include(r => r.Category)
                        .Include(r => r.WallMaterial)
                        .Include(r => r.TransactionType)
                        .FirstOrDefault(r => r.Id == id);
        }

        public override IEnumerable<Estate> GetAll()
        {
            return db.Estates
                        .Include(r => r.Counterparty)
                        .Include(r => r.Realtor)
                        .Include(r => r.Pictures)
                        .Include(r => r.Category)
                        .Include(r => r.WallMaterial)
                        .Include(r => r.TransactionType);
        }

        public override void Update(Estate item)
        {
            db.SetModified(item);
        }
    }
}