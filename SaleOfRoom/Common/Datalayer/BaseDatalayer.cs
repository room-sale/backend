﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Common.Datalayer
{
    public abstract class BaseDatalayer : IDisposable
    {
        protected bool disposed = false;

        protected ApplicationDbContext db;

        public BaseDatalayer(ApplicationDbContext context)
        {
            db = context;
        }

        public void Save(string userId)
        {
            db.SaveChanges(userId);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}