﻿using SaleOfRoom.Common.Repository;
using SaleOfRoom.Common.Repository.Base.Interface;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleOfRoom.Common.Datalayer
{
    public class Datalayer : BaseDatalayer
    {
        public Datalayer(ApplicationDbContext context)
               : base(context)
        {
        }

        private IIdentityRepository<Realtor> realtorRepository;

        public IIdentityRepository<Realtor> Realtors
        {
            get
            {
                if (realtorRepository == null)
                    realtorRepository = new RealtorRepository(db);

                return realtorRepository;
            }
        }

        private IRepository<Estate> estateRepository;

        public IRepository<Estate> Estates
        {
            get
            {
                if (estateRepository == null)
                    estateRepository = new EstateRepository(db);

                return estateRepository;
            }
        }

        private IRepository<Counterparty> counterpartyRepository;
        public IRepository<Counterparty> Counterparties
        {
            get
            {
                if (counterpartyRepository == null)
                    counterpartyRepository = new CounterpartyRepository(db);

                return counterpartyRepository;
            }
        }

        private CategoryRepository categoryRepository;
        public CategoryRepository Categories
        {
            get
            {
                if (categoryRepository == null)
                    categoryRepository = new CategoryRepository(db);

                return categoryRepository;
            }
        }

        private TransactionTypeRepository transactionTypeRepository;
        public TransactionTypeRepository TransactionTypes
        {
            get
            {
                if (transactionTypeRepository == null)
                    transactionTypeRepository = new TransactionTypeRepository(db);

                return transactionTypeRepository;
            }
        }

        private WallMaterialRepository wallMaterialRepository;
        public WallMaterialRepository WallMaterials
        {
            get
            {
                if (wallMaterialRepository == null)
                    wallMaterialRepository = new WallMaterialRepository(db);

                return wallMaterialRepository;
            }
        }

        private PictureRepository pictureRepository;
        public PictureRepository Picturies
        {
            get
            {
                if (pictureRepository == null)
                    pictureRepository = new PictureRepository(db);

                return pictureRepository;
            }
        }

        private HistoryRepository historyRepository;
        public HistoryRepository History
        {
            get
            {
                if (historyRepository == null)
                    historyRepository = new HistoryRepository(db);

                return historyRepository;
            }
        }

        private EmployeeRepository employeeRepository;
        public EmployeeRepository Employee
        {
            get
            {
                if (employeeRepository == null)
                    employeeRepository = new EmployeeRepository(db);

                return employeeRepository;
            }
        }
    }
}