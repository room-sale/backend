﻿using Microsoft.AspNet.Identity.EntityFramework;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;


namespace SaleOfRoom.Common
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //private static DbContextHelper _helper = new DbContextHelper();
        //private readonly IAuditDbContext _auditContext;

        public DbSet<Realtor> Realtors { get; set; }
        public DbSet<Counterparty> Counterparties { get; set; }
        public DbSet<Estate> Estates { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<WallMaterial> WallMaterials { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EntityLog> EntityLogs { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            //_auditContext = new DefaultAuditContext(this);
            //_helper.SetConfig(_auditContext);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public int SaveChanges(string userId)
        {
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
            {
                // For each changed record, get the audit record entries and add them
                var auditRecords = GetAuditRecordsForChange(ent, userId);
                foreach (EntityLog x in auditRecords)
                {
                    this.EntityLogs.Add(x);
                }
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();


            //List<EntityLog> listLogs = AuditTrail.GetLogEntries(this.ObjectStateManager);
            //foreach (var log in listLogs)
            //{
            //    var auditLog = new AuditLog() //fill the AuditLog entity of EF
            //    {
            //        Id = log.Id,
            //        AuditType = log.Action,
            //        TableName = log.TableName,
            //        PK = log.PrimaryKey,
            //        ColumnName = log.ColumnName,
            //        OldValue = log.OldValue,
            //        NewValue = log.NewValue,
            //        Date = log.Date,
            //        UserId = log.UserId
            //    };

            //    this.AuditLogs.AddObject(auditLog); //store audit details into DB table
            //}

            //return base.SaveChanges(options);


            //var auditEntries = ActionBeforeSaveChanges(id);
            //var result = SaveChanges();
            //ActionAfterSaveChanges(auditEntries);
            //return result;
        }

        //private List<AuditEntry> ActionBeforeSaveChanges(string id)
        //{
        //    ChangeTracker.DetectChanges();
        //    var entries = new List<AuditEntry>();
        //    foreach (var entry in ChangeTracker.Entries())
        //    {
        //        if (entry.Entity is EntityLog || entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
        //        {
        //            continue;
        //        }

        //        var auditEntry = new AuditEntry(entry)
        //        {
        //            TableName = GetTableName(entry)
        //        };
        //        entries.Add(auditEntry);
        //        SetProperties(entry, ref auditEntry);
        //        auditEntry.UserId = id;
        //    }


        //    return entries.ToList();
        //}

        //private void SetProperties(DbEntityEntry entry, ref AuditEntry auditEntry)
        //{
        //    var primaryKey = GetPrimaryKeyValue(entry);
        //    auditEntry.KeyValues[primaryKey.Key] = primaryKey.Value;

        //    foreach (var propertyName in entry.OriginalValues.PropertyNames)
        //    {
        //        var property = entry.Property(propertyName);
        //        if (property.Name == primaryKey.Key)
        //            continue;

        //        switch (entry.State)
        //        {
        //            case EntityState.Added:
        //                auditEntry.NewValues[propertyName] = property.CurrentValue;
        //                break;

        //            case EntityState.Deleted:
        //                auditEntry.OldValues[propertyName] = property.OriginalValue;
        //                break;

        //            case EntityState.Modified:
        //                if (property.IsModified)
        //                {
        //                    auditEntry.OldValues[propertyName] = property.OriginalValue;
        //                    auditEntry.NewValues[propertyName] = property.CurrentValue;
        //                }
        //                break;
        //            case EntityState.Detached:
        //                break;
        //            case EntityState.Unchanged:
        //                break;
        //        }
        //    }
        //}

        //private EntityKeyMember GetPrimaryKeyValue(DbEntityEntry entry)
        //{
        //    var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
        //    return objectStateEntry.EntityKey.EntityKeyValues[0];
        //}
        //public override int SaveChanges()
        //{
        //    return _helper.SaveChanges(_auditContext, () => base.SaveChanges());
        //}

        //public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        //{
        //    return await _helper.SaveChangesAsync(_auditContext, () => base.SaveChangesAsync(cancellationToken));
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationUser>()
                    .HasOptional(u => u.Realtor)
                    .WithRequired(c => c.User)
                    .WillCascadeOnDelete(true);
            

            base.OnModelCreating(modelBuilder);
        }

    }
}
