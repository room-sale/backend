﻿using Microsoft.AspNet.Identity.EntityFramework;
using SaleOfRoom.Common.Attributes;
using SaleOfRoom.Models.SaleOfRoom;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;

namespace SaleOfRoom.Common
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //public static List<EntityLog> GetLogEntries(ObjectStateManager entities)
        //{
        //    List<EntityLog> listLogs = new List<EntityLog>();
        //    var entries = entities.GetObjectStateEntries(EntityState.Added | EntityState.Modified | EntityState.Deleted);
        //    foreach (var entry in entries)
        //    {
        //        var tableName = entry.Entity.GetType().Name;
        //        var pk = GetPrimaryKeys(entry);
        //        if (entry.State == EntityState.Added)
        //        {
        //            var currentEntry = entities.GetObjectStateEntry(entry.EntityKey);
        //            var currentValues = currentEntry.CurrentValues;
        //            for (var i = 0; i < currentValues.FieldCount; i++)
        //            {
        //                var propName = currentValues.DataRecordInfo.FieldMetadata[i].FieldType.Name;
        //                var newValue = currentValues[propName].ToString();
        //                var log = new EntityLog()
        //                {
        //                    Action = "I",
        //                    TableName = tableName,
        //                    PrimaryKey = pk,
        //                    ColumnName = propName,
        //                    OldValue = null,
        //                    NewValue = newValue,
        //                    Date = DateTime.Now,
        //                    UserId = String.Empty
        //                };

        //                listLogs.Add(log);
        //            }
        //        }
        //        else if (entry.State == EntityState.Modified)
        //        {
        //            var currentEntry = entities.GetObjectStateEntry(entry.EntityKey);
        //            var currentValues = currentEntry.CurrentValues;
        //            var originalValues = currentEntry.OriginalValues;
        //            var properties = currentEntry.GetModifiedProperties();
        //            foreach (var propName in properties)
        //            {
        //                var oldValue = originalValues[propName].ToString();
        //                var newValue = currentValues[propName].ToString();
        //                if (oldValue == newValue) 
        //                    continue;
        //                var log = new EntityLog()
        //                {
        //                    Action = "M",
        //                    TableName = tableName,
        //                    PrimaryKey = pk,
        //                    ColumnName = propName,
        //                    OldValue = oldValue,
        //                    NewValue = newValue,
        //                    Date = DateTime.Now,
        //                    UserId = String.Empty
        //                };

        //                listLogs.Add(log);
        //            }
        //        }
        //        else if (entry.State == EntityState.Deleted)
        //        {
        //            var currentEntry = entities.GetObjectStateEntry(entry.EntityKey);
        //            var originalValues = currentEntry.OriginalValues;
        //            for (var i = 0; i < originalValues.FieldCount; i++)
        //            {
        //                var oldValue = originalValues[i].ToString();
        //                var log = new EntityLog()
        //                {
        //                    Action = "D",
        //                    TableName = tableName,
        //                    PrimaryKey = pk,
        //                    ColumnName = null,
        //                    OldValue = oldValue,
        //                    NewValue = null,
        //                    Date = DateTime.Now,
        //                    UserId = String.Empty
        //                };

        //                listLogs.Add(log);
        //            }
        //        }
        //    }

        //    return listLogs;
        //}

        //private static string GetPrimaryKeys(ObjectStateEntry entry)
        //{
        //    string pk = string.Empty;

        //    if (entry.EntityKey == null 
        //        || entry.EntityKey.EntityKeyValues == null 
        //        || entry.EntityKey.EntityKeyValues.Length == 0) 
        //        return "N/A";

        //    foreach (var keyValue in entry.EntityKey.EntityKeyValues)
        //        pk += string.Format("{0}={1};", keyValue.Key, keyValue.Value);

        //    return pk;
        //}

        private List<EntityLog> GetAuditRecordsForChange(DbEntityEntry dbEntry, string userId)
        {
            List<EntityLog> result = new List<EntityLog>();
            
            DateTime changeTime = DateTime.UtcNow;
            TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), true).SingleOrDefault() as TableAttribute;
            string tableName = GetTableName(dbEntry);
            
            var keyNames = dbEntry.Entity.GetType().GetProperties().Where(p => p.GetCustomAttribute<Ignore>() == null).ToList();
            string keyName = keyNames[0].Name;
            
            var jsonSerializer = new JavaScriptSerializer();

            if (dbEntry.State == EntityState.Added)
            {
                IList<string> columns = new List<string>();
                IList<string> newValues = new List<string>();
                foreach (string propertyName in dbEntry.CurrentValues.PropertyNames)
                {
                    columns.Add(propertyName);
                    newValues.Add(dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString());
                }
                result.Add(new EntityLog()
                {
                    UserId = userId,
                    Date = changeTime,
                    Action = "Added",    
                    TableName = tableName,
                    PrimaryKey = dbEntry.CurrentValues.GetValue<object>(keyName).ToString(),
                    ColumnName = jsonSerializer.Serialize(columns),
                    NewValue = jsonSerializer.Serialize(newValues)
                });
            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                result.Add(new EntityLog()
                {
                    UserId = userId,
                    Date = changeTime,
                    Action = "Deleted", 
                    TableName = tableName,
                    PrimaryKey = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                    ColumnName = "*ALL",
                    NewValue = jsonSerializer.Serialize(dbEntry.OriginalValues.ToObject())
                });
            }
            else if (dbEntry.State == EntityState.Modified)
            {
                IList<string> columns = new List<string>();
                IList<string> newValues = new List<string>();
                IList<string> oldValues = new List<string>();
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    if (!object.Equals(dbEntry.OriginalValues.GetValue<object>(propertyName), dbEntry.CurrentValues.GetValue<object>(propertyName)))
                    {
                        columns.Add(propertyName);
                        newValues.Add(dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString());
                        oldValues.Add(dbEntry.OriginalValues.GetValue<object>(propertyName) == null ? null : dbEntry.OriginalValues.GetValue<object>(propertyName).ToString());
                    }
                }
                result.Add(new EntityLog()
                {
                    UserId = userId,
                    Date = changeTime,
                    Action = "Modified",  
                    TableName = tableName,
                    PrimaryKey = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                    ColumnName = jsonSerializer.Serialize(columns),
                    OldValue = jsonSerializer.Serialize(oldValues),
                    NewValue = jsonSerializer.Serialize(newValues)
                });
            }
            
            return result;
        }

        private string GetTableName(DbEntityEntry ent)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)this).ObjectContext;
            Type entityType = ent.Entity.GetType();

            if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
                entityType = entityType.BaseType;

            string entityTypeName = entityType.Name;

            EntityContainer container =
                objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName, DataSpace.CSpace);
            string entitySetName = (from meta in container.BaseEntitySets
                                    where meta.ElementType.Name == entityTypeName
                                    select meta.Name).First();
            return entitySetName;
        }
    }
}