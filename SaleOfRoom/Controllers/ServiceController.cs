﻿using Microsoft.Owin.Security;
using SaleOfRoom.Common;
using SaleOfRoom.Common.Datalayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    [RoutePrefix("api/service")]
    public class ServiceController : BaseController
    {
        public ServiceController()
            : base() { }
        public ServiceController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
            : base() { }

        [Route("categories")]
        [HttpGet]
        public IHttpActionResult GetCategories()
        {
            return Json(Datalayer.Categories.GetAll());
        }

        [Route("wall-materials")]
        [HttpGet]
        public IHttpActionResult GetWallMaterials()
        {
            return Json(Datalayer.WallMaterials.GetAll());
        }

        [Route("transaction-types")]
        [HttpGet]
        public IHttpActionResult GetTransactionTypes()
        {
            return Json(Datalayer.TransactionTypes.GetAll());
        }

        [Route("image")]
        [HttpGet]
        public HttpResponseMessage GetImage(Guid id)
        {
            var img = Datalayer.Picturies.Get(id);
            if (img == null)
                return null;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(img.Image)
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = img.Id + ".png"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

            return result;
        }

        [Route("ready-state")]
        [HttpGet]
        public IHttpActionResult GetReadyStates()
            => Json(new string[] { "Холодно", "Тепло", "Горячо" });

        [Route("status")]
        [HttpGet]
        public IHttpActionResult GetStatuses()
            => Json(new string[] { "Потенциальный", "По договору", "Задаток", "Сделка" });

        [Route("room-type")]
        [HttpGet]
        public IHttpActionResult GetRoomTypes()
            => Json(new string[] { "Смежные", "Изолированные" });

        [Route("estate-state")]
        [HttpGet]
        public IHttpActionResult GetEstateStates()
            => Json(new string[] { "Отличное", "Хорошее", "Нормальное", "Плохое" });

        [Route("repair")]
        [HttpGet]
        public IHttpActionResult GetRepairs()
            => Json(new string[] { "Евро", "Дизайнерский", "Частичный ремонт", "Хороший", "С отделкой", "Черновая отделка", "Требуется ремонт" });

        [Route("deal-statuses")]
        [HttpGet]
        public IHttpActionResult DealStatus()
            => Json(new string[] { "первичная продажа", "переуступка", "прямая продажа", "первичная продажа вторички", "встречная продажа" });
    
        [Route("positions")]
        [HttpGet]
        public IHttpActionResult Positions()
            => Json(new string[] { "Генеральный директор", "Секретарь", "Риэлтор" });
    }
}