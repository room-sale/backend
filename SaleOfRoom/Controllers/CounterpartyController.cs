﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SaleOfRoom.Common;
using SaleOfRoom.Common.Datalayer;
using SaleOfRoom.Models.BindingModel;
using SaleOfRoom.Models.SaleOfRoom;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    [Authorize(Roles = "Admin,Realtor")]
    [RoutePrefix("api/counterparty")]
    public class CounterpartyController : BaseController
    {
        public CounterpartyController()
            : base() { }
        public CounterpartyController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
            : base() { }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetCounterparties(int? page = 1, string searchString = null, bool isDisabled = false)
        {
            var items = Datalayer.Counterparties.GetAll().Where(c => c.isDisabled == isDisabled);
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("Realtor") && (searchString == null || searchString == string.Empty))
                    items = items.Where(c => c.Estates.Where(e => e.UserId == User.Identity.GetUserId()).ToArray().Length > 0 );

            if (searchString != null)
                items = items
                            .Where(c =>
                                    c.FirstName == searchString
                                    || c.SecondName == searchString
                                    || c.LastName == searchString
                                    || c.FirstName + c.SecondName + c.LastName == searchString
                                    || c.Phone.Contains(searchString));

            var result = Datalayer.Counterparties
                                            .ToPagedList(items, page.Value, 25)
                                            .Select(x => x.ToIndexViewModel());

            return Json(new CounterpartyPageViewModel()
            {
                Counterparties = result,
                CounterpartyCount = items.Count()
            });
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetCounterparty(Guid id)
        {
            var counterparty = Datalayer.Counterparties.Get(id);
            
            if (counterparty == null || counterparty.isDisabled)
                return NotFound();

            return Json(counterparty.ToViewModel());
        }

        [Authorize(Roles = "Admin,Realtor")]
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post(AddCounterpartyBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var counterparty = new Counterparty() 
            {
                FirstName = model.FirstName,
                SecondName = model.SecondName,
                LastName = model.LastName,
                Phone = model.Phone
            };

            Datalayer.Counterparties.Create(counterparty);
            Datalayer.Save(User.Identity.GetUserId());

            return Json(counterparty.ToIndexViewModel());
        }

        [Authorize(Roles = "Admin,Realtor")]
        [Route("")]
        [HttpPut]
        public IHttpActionResult Put(UpdateCounterpartyBindingModel model, Guid id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var counterparty = Datalayer.Counterparties.Get(id);

            if (counterparty.isDisabled)
                return NotFound();

            counterparty.FirstName = model.FirstName;
            counterparty.SecondName = model.SecondName;
            counterparty.LastName = model.LastName;
            counterparty.Phone = model.Phone;

            Datalayer.Counterparties.Update(counterparty);
            Datalayer.Save(User.Identity.GetUserId());

            return Json(counterparty.ToViewModel());
        }

        [Authorize(Roles = "Admin,Realtor")]
        [Route("")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            var counterparty = Datalayer.Counterparties.Get(id);
            if (counterparty == null)
                return NotFound();

            Datalayer.Counterparties.Delete(counterparty.Id);
            string ida = UserManager.FindById(User.Identity.GetUserId()).Id;
            Datalayer.Save(ida);

            return Ok();
        }
    }
}
