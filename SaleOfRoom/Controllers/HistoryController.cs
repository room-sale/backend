﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaleOfRoom.Common;
using SaleOfRoom.Common.Datalayer;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    [Authorize(Roles = "Admin,Realtor")]
    [RoutePrefix("api/history")]
    public class HistoryController : BaseController
    {
        public HistoryController()
            : base() { }
        public HistoryController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
            : base() { }

        [AllowAnonymous]
        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetAll(string tableName, int? page = 1)
        {
            var items = Datalayer.History.GetAll(tableName);

            var result = Datalayer.History
                                    .ToPagedList(items, page.Value, 25)
                                    .Select(x => x.ToIndexViewModel(UserManager.GetRolesAsync(x.UserId).Result[0]));

            return Json(new HistoryPageViewModel()
            {
                History = result,
                HistoryCount = items.Count()
            });
        }
    }
}
