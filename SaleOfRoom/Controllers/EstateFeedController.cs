﻿using SaleOfRoom.Common.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    [RoutePrefix("api/feed")]
    public class EstateFeedController : BaseController
    {
        [AllowAnonymous]
        [Route("dom-click")]
        [HttpGet]
        public HttpResponseMessage GetDomClick()
        {
            var items = Datalayer.Estates.GetAll().Where(e => e.isDisabled == false && e.OnDomClick == true);
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(EstateConverter.GetEstateYRL(items.ToArray()))
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

            return result;
        }

        [AllowAnonymous]
        [Route("mir-kvartir")]
        [HttpGet]
        public HttpResponseMessage GetMirKvartir()
        {
            var items = Datalayer.Estates.GetAll().Where(e => e.isDisabled == false && e.OnMirKvartir == true);
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(EstateConverter.GetEstateYRL(items.ToArray()))
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

            return result;
        }

        [AllowAnonymous]
        [Route("gde-etot-dom")]
        [HttpGet]
        public HttpResponseMessage GetGdeEtotDom()
        {
            var items = Datalayer.Estates.GetAll().Where(e => e.isDisabled == false && e.OnGdeEtotDom == true);
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(EstateConverter.GetEstateYRL(items.ToArray()))
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

            return result;
        }

        [AllowAnonymous]
        [Route("restat")]
        [HttpGet]
        public HttpResponseMessage GetRestat()
        {
            var items = Datalayer.Estates.GetAll().Where(e => e.isDisabled == false && e.OnRestat == true);
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(EstateConverter.GetEstateYRL(items.ToArray()))
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

            return result;
        }
    }
}
