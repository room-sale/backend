﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using SaleOfRoom.Common;
using SaleOfRoom.Common.Datalayer;
using SaleOfRoom.Models.BindingModel;
using SaleOfRoom.Models.SaleOfRoom;
using SaleOfRoom.Models.ViewModel;
using SaleOfRoom.Providers;
using SaleOfRoom.Results;

namespace SaleOfRoom.Controllers
{
    [Authorize(Roles = "Admin,Realtor")]
    [RoutePrefix("api/account")]
    public class AccountController : BaseController
    {
        public AccountController()
            : base() { }
        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
            : base() { }

        // GET api/Account/user-info
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("user-info")]
        public UserInfoViewModel GetUserInfo()
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());

            if (user.isDisabled)
                return null;

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                Role = UserManager.GetRolesAsync(user.Id).Result.FirstOrDefault(),
                Id = User.Identity.GetUserId()
            };
        }

        [Authorize(Roles = "Admin")]
        [Route("users")]
        [HttpGet]
        public IHttpActionResult GetUsers(int? page, string searchString = null, bool isDisabled = false)
        {
            var users = UserManager.Users.Where(u => u.isDisabled == isDisabled);

            if (searchString != null)
                users = users.Where(u => u.Email == searchString);

            var result = users
                            .AsEnumerable()
                            .OrderBy(u => u.Email)
                            .Skip(25 * (page.Value - 1))
                            .Take(25)
                            .Select(x => new IndexUserViewModel()
                            {
                                Id = x.Id,
                                Email = x.Email,
                                Name = x?.Realtor?.SecondName + " " + x?.Realtor?.FirstName + " " + x?.Realtor?.LastName,
                                Role = UserManager.GetRolesAsync(x.Id).Result[0],
                            });

            return Ok(new UserPageViewModel()
            {
                Users = result,
                UsersCount = users.Count()
            });

        }

        [Authorize(Roles = "Admin")]
        [Route("user")]
        [HttpGet]
        public IHttpActionResult GetUser(string id)
        {
            ApplicationUser user = UserManager.FindById(id);
            
            if (user == null)
                return NotFound();

            return Ok(user.ToViewModel(UserManager.GetRolesAsync(user.Id).Result[0]));
        }

        // POST api/account/logout
        [Route("logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // POST api/account/change-password
        [Route("change-password")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/set-password
        [Route("set-password")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/account/register
        [Authorize(Roles = "Admin")]
        [Route("register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model, bool? isAdmin = false)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };


            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            
            var currentUser = UserManager.FindByEmail(user.Email);

            if (isAdmin.HasValue && isAdmin.Value == false)
            {
                UserManager.AddToRole(currentUser.Id, "Realtor");

                var realtor = new Realtor()
                {
                    FirstName = model.FirstName,
                    SecondName = model.SecondName,
                    LastName = model.LastName,
                    Phone = model.Phone,
                    UserId = currentUser.Id
                };

                Datalayer.Realtors.Create(realtor);
                Datalayer.Save(User.Identity.GetUserId());
            }
            else
            {
                UserManager.AddToRole(currentUser.Id, "Admin");
            }

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [Route("")]
        [HttpPut]
        public IHttpActionResult Put(EditUserBindingModel model, string id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            ApplicationUser user = UserManager.FindById(id);
            if (user.isDisabled)
                return NotFound();

            if(user.Realtor != null)
            {
                var r = Datalayer.Realtors.Get(user.Realtor.UserId);
                r.FirstName = model.FirstName;
                r.SecondName = model.SecondName;
                r.LastName = model.LastName;
                r.Phone = model.Phone;

                Datalayer.Realtors.Update(r);
                Datalayer.Save(User.Identity.GetUserId());
            }
            

            user.UserName = model.Username;
            user.Email = model.Email;

            UserManager.Update(user);

            return Ok();

        }

        [Authorize(Roles = "Admin")]
        [Route("")]
        [HttpDelete]
        public IHttpActionResult Delete(string id)
        {
            var user = UserManager.FindById(id);
            if (user == null)
                return NotFound();

            user.isDisabled = !user.isDisabled;

            UserManager.Update(user);

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
