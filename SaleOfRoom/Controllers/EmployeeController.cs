﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SaleOfRoom.Models.BindingModel;
using SaleOfRoom.Models.SaleOfRoom;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    [RoutePrefix("api/employee")]
    public class EmployeeController : BaseController
    {
        public EmployeeController()
            : base() { }
        public EmployeeController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
            : base() { }

        [AllowAnonymous]
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(int? page = 1)
        {
            var items = Datalayer.Employee.GetAll();
            
            var result = Datalayer.Employee
                                    .ToPagedList(items, page.Value, 25)
                                    .Select(x => x.ToEmployeeIndexViewModel());

            return Json(new EmployeePageViewModel()
            {
                Employees = result,
                EmployeeCount = items.Count()
            });
        }

        [AllowAnonymous]
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetCurrent(Guid id)
        {
            var employee = Datalayer.Employee.Get(id);
            
            return Json(employee.ToEmployeeViewModel());
        }

        [Route("")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IHttpActionResult Post(AddEmployeeBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var employee = new Employee()
            {
                FirstName = model.FirstName,
                SecondName = model.SecondName,
                LastName = model.LastName,
                Position = model.Position
            };

            Datalayer.Employee.Create(employee);
            Datalayer.Save(User.Identity.GetUserId());

            return Json(Datalayer.Employee.Get(employee.Id).ToEmployeeIndexViewModel());
        }

        [Route("set-image")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IHttpActionResult> SetImage(Guid id)
        {
            if (!Request.Content.IsMimeMultipartContent())
                return BadRequest();

            var employee = Datalayer.Employee.Get(id);
            if (employee == null)
                return NotFound();
            
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider).ContinueWith(async (task) =>
            {
                var p = task.Result;

                var picture = p.Contents[0];
                
                byte[] pictureData = await picture.ReadAsByteArrayAsync();

                employee.Picture = new Picture() { Image = pictureData };
                
            });

            Datalayer.Employee.Update(employee);
            Datalayer.Save(User.Identity.GetUserId());

            return Ok();
        }

        [Route("")]
        [Authorize(Roles = "Admin")]
        [HttpPut]
        public IHttpActionResult Put(EmployeeEditBindingModel model, Guid id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var employee = Datalayer.Employee.Get(id);
            if (employee == null)
                return NotFound();
            
            if (model.DeleteImage.HasValue && model.DeleteImage.Value == true)
            {
                Datalayer.Picturies.Delete(employee.PictureId.Value);
                Datalayer.Save(User.Identity.GetUserId());
            }

            employee.FirstName = model.FirstName;
            employee.SecondName = model.SecondName;
            employee.LastName = model.LastName;
            employee.Position = model.Position;

            Datalayer.Employee.Update(employee);
            Datalayer.Save(User.Identity.GetUserId());

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [Route("")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            var employee = Datalayer.Employee.Get(id);
            if (employee == null)
                return NotFound();

            Datalayer.Employee.Delete(employee.Id);

            Datalayer.Save(User.Identity.GetUserId());

            return Ok();
        }
    }
}
