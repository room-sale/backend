﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SaleOfRoom.Common;
using SaleOfRoom.Common.Datalayer;
using SaleOfRoom.Models.BindingModel;
using SaleOfRoom.Models.SaleOfRoom;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    [Authorize(Roles = "Admin,Realtor")]
    [RoutePrefix("api/estate")]
    public class EstateController : BaseController
    {
        public EstateController()
            : base() { }
        public EstateController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
            : base() { }

        [AllowAnonymous]
        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetAll(int? page = 1, bool isDisabled = false, bool isTable = false, bool my = false)
        {
            var items = Datalayer.Estates.GetAll().Where(e => e.isDisabled == isDisabled);
            
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("Realtor") && my)
                    items = items.Where(e => e.UserId == User.Identity.GetUserId());

            object result;
            bool isRealtor = false, isAdmin = false;
            if (User.IsInRole("Realtor"))
                isRealtor = true;
            if (User.IsInRole("Admin"))
                isAdmin = true;
            if (isTable)
                result = Datalayer.Estates
                                        .ToPagedList(items, page.Value, 25)
                                        .Select(x => x.ToEstateTableViewModel());
            else
                result = Datalayer.Estates
                                        .ToPagedList(items, page.Value, 25)
                                        .Select(x => x.ToIndexViewModel(isRealtor, isAdmin, User.Identity.GetUserId()));

            return Json(new EstatePageViewModel()
            {
                Estates = result,
                EstateCount = items.Count()
            });
        }

        [AllowAnonymous]
        [Route("all")]
        [HttpPost]
        public IHttpActionResult GetAll(Filter filter, int? page = 1, bool isDisabled = false, bool my = false, bool isTable = false)
        {
            var items = Datalayer.Estates.GetAll().Where(e => e.isDisabled == isDisabled);

            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("Realtor") && my)
                    items = items.Where(e => e.UserId == User.Identity.GetUserId());

            if (filter != null)
                Estate.AplyFilter(filter, ref items);

            object result;
            bool isRealtor = false, isAdmin = false;
            if (User.IsInRole("Realtor"))
                isRealtor = true;
            if (User.IsInRole("Admin"))
                isAdmin = true;
            if (isTable)
                result = Datalayer.Estates
                                        .ToPagedList(items, page.Value, 25)
                                        .Select(x => x.ToEstateTableViewModel());
            else
                result = Datalayer.Estates
                                        .ToPagedList(items, page.Value, 25)
                                        .Select(x => x.ToIndexViewModel(isRealtor, isAdmin, User.Identity.GetUserId()));

            return Json(new EstatePageViewModel()
            {
                Estates = result,
                EstateCount = items.Count()
            });
        }


        [Route(Name = "")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            var estate = Datalayer.Estates.Get(id);
            if (estate == null || estate.isDisabled)
                return NotFound();

            bool isRealtor = false, isAdmin = false;
            if (User.IsInRole("Realtor"))
                isRealtor = true;
            if (User.IsInRole("Admin"))
                isAdmin = true;
            return Json(estate.ToViewModel(isRealtor, isAdmin, User.Identity.GetUserId()));
        }

        [Route("")]
        [Authorize(Roles = "Admin,Realtor")]
        [HttpPost]
        public IHttpActionResult Post(AddEstateBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var estate = new Estate()
            {
                WallMaterialId = model.WallMaterialId,
                Area = model.Area,
                CategoryId = model.CategoryId,
                City = model.City,
                CityDistrict = model.CityDistrict,
                Comment = model.Comment,
                CounterpartyId = model.CounterpartyId.Value,
                CreatedAt = DateTime.Now,
                District = model.District,
                FlatNumber = model.FlatNumber,
                Floor = model.Floor,
                HouseNumber = model.HouseNumber,
                HousingNumber = model.HousingNumber,
                KitchenArea = model.KitchenArea,
                LivingArea = model.LivingArea,
                Price = model.Price,
                ReadyState = model.ReadyState,
                UserId = model.RealtorId,
                Region = model.Region,
                RoomCount = model.RoomCount,
                Street = model.Street,
                TransactionTypeId = model.TransactionTypeId,
                Coord = model.Coord,
                PriceArea = model.Price / model.Area,
                StoreysNumber = model.StoreysNumber,
                Status = model.Status,
                RommType = model.RommType,
                Repairs = model.Repairs,
                Commission = model.Commission,
                CadastralNumber = model.CadastralNumber,
                Header = model.Header,
                Message = model.Message,
                DealStatus = model.DealStatus,
                OnDomClick = model.OnDomClick,
                OnGdeEtotDom = model.OnGdeEtotDom,
                OnMirKvartir = model.OnMirKvartir,
                OnRestat = model.OnRestat
            };

            Datalayer.Estates.Create(estate);
            Datalayer.Save(User.Identity.GetUserId());
            bool isRealtor = false, isAdmin = false;
            if (User.IsInRole("Realtor"))
                isRealtor = true;
            if (User.IsInRole("Admin"))
                isAdmin = true;
            return Json(Datalayer.Estates.Get(estate.Id).ToIndexViewModel(isRealtor, isAdmin, User.Identity.GetUserId()));
        }

        [Route("set-images")]
        [Authorize(Roles = "Admin,Realtor")]
        [HttpPost]
        public async Task<IHttpActionResult> SetImages(Guid id)
        {
            if (!Request.Content.IsMimeMultipartContent())
                return BadRequest();
            
            var estate = Datalayer.Estates.Get(id);
            if (estate == null || estate.isDisabled)
                return NotFound();

            if (User.IsInRole("Realtor") && User.Identity.GetUserId() != estate.UserId)
                return Unauthorized();

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider).ContinueWith(async (task) =>
            {
                var p = task.Result;

                foreach (var picture in p.Contents)
                {
                    byte[] pictureData = await picture.ReadAsByteArrayAsync();

                    estate.Pictures.Add(new Picture() { Image = pictureData });
                }
            });

            Datalayer.Estates.Update(estate);
            Datalayer.Save(User.Identity.GetUserId());

            return Ok();
        }

        [Route("")]
        [Authorize(Roles = "Admin,Realtor")]
        [HttpPut]
        public IHttpActionResult Put(EstateEditBindingModel model, Guid id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var estate = Datalayer.Estates.Get(id);
            if (estate == null || estate.isDisabled)
                return NotFound();

            var user = UserManager.FindById(User.Identity.GetUserId());
            bool? realtorDisabled = user?.Realtor?.isDisabled;
            if ((realtorDisabled.HasValue && realtorDisabled.Value) || user.isDisabled)
                return Unauthorized();

            if (User.IsInRole("Realtor") && user.Id != estate.UserId)
                return Unauthorized();

            if (model.DeletedImages != null)
            {
                foreach (var picrure in model.DeletedImages)
                    Datalayer.Picturies.Delete(picrure);
                Datalayer.Save(user.Id);
            }

            estate.Area = model.Area;
            estate.CategoryId = model.CategoryId;
            estate.City = model.City;
            estate.CityDistrict = model.CityDistrict;
            estate.Comment = model.Comment;
            estate.Coord = model.Coord;
            estate.CounterpartyId = model.CounterpartyId;
            estate.District = model.District;
            estate.FlatNumber = model.FlatNumber;
            estate.Floor = model.Floor;
            estate.HouseNumber = model.HouseNumber;
            estate.HousingNumber = model.HousingNumber;
            estate.KitchenArea = model.KitchenArea;
            estate.LivingArea = model.LivingArea;
            estate.Price = model.Price;
            if (model.PriceArea != null || model.PriceArea != 0)
                estate.PriceArea = model.PriceArea;
            estate.ReadyState = model.ReadyState;
            estate.UserId = model.RealtorId;
            estate.Region = model.Region;
            estate.RoomCount = model.RoomCount;
            estate.Street = model.Street;
            estate.TransactionTypeId = model.TransactionTypeId;
            estate.WallMaterialId = model.WallMaterialId;
            estate.StoreysNumber = model.StoreysNumber;
            estate.Status = model.Status;
            estate.RommType = model.RommType;
            estate.Repairs = model.Repairs;
            estate.Commission = model.Commission;
            estate.CadastralNumber = model.CadastralNumber;
            estate.Header = model.Header;
            estate.Message = model.Message;
            estate.DealStatus = model.DealStatus;
            estate.OnDomClick = model.OnDomClick;
            estate.OnGdeEtotDom = model.OnGdeEtotDom;
            estate.OnMirKvartir = model.OnMirKvartir;
            estate.OnRestat = model.OnRestat;

            Datalayer.Estates.Update(estate);
            Datalayer.Save(user.Id);

            return Ok();
        }

        [Route("")]
        [Authorize(Roles = "Admin,Realtor")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            var estate = Datalayer.Estates.Get(id);
            if (estate == null)
                return NotFound();

            Datalayer.Estates.Delete(id);
            Datalayer.Save(User.Identity.GetUserId());

            return Ok();
        }
    }
}
