﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SaleOfRoom.Common;
using SaleOfRoom.Common.Datalayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    public class BaseController : ApiController
    {
        protected ApplicationUserManager _userManager;
        protected Datalayer Datalayer;
        
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        public BaseController()
        {
            Datalayer = new Datalayer(new ApplicationDbContext());
        }

        public BaseController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}
