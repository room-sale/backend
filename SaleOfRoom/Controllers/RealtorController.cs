﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SaleOfRoom.Common;
using SaleOfRoom.Common.Datalayer;
using SaleOfRoom.Models.BindingModel;
using SaleOfRoom.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SaleOfRoom.Controllers
{
    [Authorize(Roles = "Admin,Realtor")]
    [RoutePrefix("api/realtor")]
    public class RealtorController : BaseController
    {
        public RealtorController()
            : base() { }
        public RealtorController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
            : base() { }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetRealtors(int? page = 1, string searchString = null, bool isDisabled = false)
        {
            var items = Datalayer.Realtors.GetAll().Where(c => c.isDisabled == isDisabled);
            if (searchString != null)
                items = items
                            .Where(c =>
                                    c.FirstName.ToLower().Contains(searchString)
                                    || c.SecondName.ToLower().Contains(searchString)
                                    || c.LastName.ToLower().Contains(searchString)
                                    || (c.FirstName + c.SecondName + c.LastName).ToLower().Contains(searchString));

            var result = Datalayer.Realtors
                                    .ToPagedList(items, page.Value, 25)
                                    .Select(x => x.ToIndexViewModel());

            return Json(new RealtorPageViewModel()
            {
                Realtors = result,
                RealtorCount = items.Count()
            });
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetRealtor(string id)
        {
            var realtor = Datalayer.Realtors.Get(id);
            if (realtor == null)
                return NotFound();

            return Json(realtor.ToViewModel());
        }
        
        [Authorize(Roles = "Admin")]
        [Route("")]
        [HttpDelete]
        public IHttpActionResult Delete(string id)
        {
            var realtor = Datalayer.Realtors.Get(id);
            if (realtor == null)
                return NotFound();

            Datalayer.Realtors.Delete(realtor.UserId);
            //foreach (var e in realtor.Estates)
            //    e.isDisabled = realtor.isDisabled;

            Datalayer.Save(User.Identity.GetUserId());

            return Ok();
        }
    }
}
